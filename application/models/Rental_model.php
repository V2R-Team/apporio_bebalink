<?php
class Rental_model extends CI_Model{
					
    	 
function Rental_Package($city_id)
    {
        $data = $this->db->select('*')
            ->from('rental_category')
            ->join('rentcard','rentcard.rental_category_id=rental_category.rental_category_id','inner')
            ->where(['rentcard.city_id'=>$city_id])
            ->get();
        return $data->result_array();
    }
    
function Rental_Package_confirm($city_id,$rental_category_id)
    {
        $data = $this->db->select('*')
            ->from('rental_category')
            ->join('rentcard','rentcard.rental_category_id=rental_category.rental_category_id','inner')
            ->where(['rentcard.city_id'=>$city_id,'rental_category.rental_category_id'=>$rental_category_id])
            ->get();
        return $data->row_array();
    }
    
        
function Rental_Pakage_Car($city_id,$rental_category_id)
    {
        $data = $this->db->select('*')
            ->from('rentcard')
            ->join('car_type','car_type.car_type_id=rentcard.car_type_id','inner')
            ->where(['rentcard.city_id'=>$city_id,'rentcard.rental_category_id'=>$rental_category_id])
            ->get();
        return $data->result_array();
    }	
function Rental_Pakage_Carconfirm($city_id,$rental_category_id)
    {
        $data = $this->db->select('*')
            ->from('rentcard')
            ->join('car_type','car_type.car_type_id=rentcard.car_type_id','inner')
            ->where(['rentcard.city_id'=>$city_id,'rentcard.rental_category_id'=>$rental_category_id])
            ->get();
        return $data->row_array();
    }		
function booking_later_rental($data,$user_id)
    {
        $this->db->insert('rental_booking',$data);
        $id = $this->db->insert_id();
        $this->db->insert('table_user_rides',['booking_id'=>$id,'ride_mode'=>2,'user_id'=>$user_id]);
    }			  

 function nearest_driver($car_type_id)
    {
        $data = $this->db->select('*')
            ->where(['online_offline'=>1,'busy'=>0,'login_logout'=>1,'car_type_id'=>$car_type_id,'driver_admin_status'=>1])
			->where(['current_lat !='=>"",'current_long !='=>""])
            ->get('driver');
        return $data->result_array();

    }

    function booking_now($data)
    {
        $this->db->insert('rental_booking',$data);
        $id = $this->db->insert_id();
        $data = $this->db->get_where('rental_booking',['rental_booking_id'=>$id]);
        return $data->row();
    
    }
    
       function booking_allocated($rental_booking_id,$driver_id,$user_id)
    {
        $this->db->insert('booking_allocated',['rental_booking_id'=>$rental_booking_id,'driver_id'=>$driver_id]);
        $this->db->insert('table_user_rides',['booking_id'=>$rental_booking_id,'ride_mode'=>2,'user_id'=>$user_id]);
    }
    
    
    
    function driver_profile($driver_id)
    {
        $data = $this->db->select('*')
            ->from('driver')
            ->join('car_type','car_type.car_type_id=driver.car_type_id','inner')
            ->join('car_model','car_model.car_model_id=driver.car_model_id','inner')
            ->where(['driver.driver_id'=>$driver_id])
            ->get();
        return $data->row();
    }  

public function get_ridedetails($last_id){
        		             $query = $this->db->select('*')
        					 	->from('rental_booking')
        					 	->where(['rental_booking_id' => $last_id])
        					 	->get();
        			     return $query->row_array();
    				     }    
    
   function admin_settings()
    {
        $data = $this->db->select('*')
                        ->from('admin_panel_settings')
                        ->where(['admin_panel_setting_id'=>1])
                        ->get();
        return $data->row();
    }
    
    function ride_status_change($rental_booking_id,$text)
    {
        $this->db->where('rental_booking_id',$rental_booking_id)
            ->update('rental_booking',$text);
        $data = $this->db->select('*')
            ->from('rental_booking')
            ->join('user','user.user_id=rental_booking.user_id','inner')
            ->where('rental_booking.rental_booking_id',$rental_booking_id)
            ->get();
        return $data->row();
    }
    
  function user_device($user_id)
    {
        $data = $this->db->select('*')
            ->where(['user_id'=>$user_id,'login_logout'=>1])
            ->get('user_device');
        return $data->result();

    }
    
           							
}
































