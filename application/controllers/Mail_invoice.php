<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
date_default_timezone_set("Asia/Kolkata");
class Mail_invoice extends CI_Controller {

		function __construct(){
				        parent::__construct();   
				        $this->load->model('Booking_model');
				       }
public function index()
{

$ride_id = base64_decode(urldecode($this->input->get('id')));
$last_id=$ride_id;
$user_email = base64_decode(urldecode($this->input->get('em')));
$type = base64_decode(urldecode($this->input->get('type')));

 /*
$ride_id = $this->input->post('id');
$last_id=$ride_id;
$user_email =$this->input->post('em');
$type = $this->input->post('type'); */


$user_id = $this->session->userdata('user_id'); 
if($type == 1)
{
$list=$this->Booking_model->get_ridedetails($last_id);
	 
    if (!empty($list))
    {
        $user_id = $list['user_id'];
        $car_type_id = $list['car_type_id'];
	$payment_date_time = $list['ride_date'];
        $payment_option_id = $list['payment_option_id'];
        $list1234=$this->Booking_model->paymentmail($payment_option_id);
        $payment_method = $list1234['payment_option_name'];
	$list122=$this->Booking_model->done_rideinfomail($ride_id);
        $distance = $list122['distance'];
        $payment_amount = $list122['total_amount'];
        $begin_time = $list122['begin_time'];
        $begin_location = $list122['begin_location'];
        $end_location = $list122['end_location'];
        $end_time = $list122['end_time'];
        $amount = $list122['amount'];
	$list12=$this->Booking_model->Get_user_details($user_id);
        $user_name = $list12['user_name'];
        $user_image = $list12['user_image'];
	$list121=$this->Booking_model->Get_car_type_mail($car_type_id);
        $car_type_name = $list121['car_type_name'];
	$subject = 'Taxi App Payment';
        $from = 'hello@apporio.com';
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: '.$from."\r\n".
            'Reply-To: '.$from."\r\n" .
            'X-Mailer: PHP/' . phpversion();

        $message = '
		<div class="gmail_quote">
  <div dir="ltr">
    <div class="gmail_quote">
      <br>                                   
      <div style="margin:0;padding:0">   
        <table bgcolor="#eeeeee" border="0" cellpadding="0" cellspacing="0" width="100%">     
          <tbody>
             
          </tbody>
        </table>  
        <table bgcolor="#eeeeee" border="0" cellpadding="0" cellspacing="0" width="100%">     
          <tbody>
            <tr>         
              <td bgcolor="#EEEEEE" align="center" style="padding:0 15px 0 15px" class="tracking-space">             
                <table border="0" cellpadding="0" cellspacing="0" style="margin:15px" width="660" class="responsive-table">                 
                  <tbody>
                    <tr>
                    	<td bgcolor="#ffffff">
                           <table width="100%">
			        <tr>
			          <td style="text-align:left;padding:10px;"> '.$payment_date_time.' </td>
			          <td style="text-align:right;padding:10px;"> Taxi App  </td>
			        </tr>
			        <tr>
			          <td colspan="2" style="text-align:center; padding:20px 0px 0px 0px; font-size:46px; font-weight: bold;"> Rs. '.round($payment_amount).'  </td>
			        </tr>
			        <tr>
			          <td colspan="2" style="text-align:center; color:#707070; padding:20px 0px 0px 0px; font-size:14px;"> CRN530048250  </td>
			        </tr>
			        <tr>
			          <td colspan="2" style="text-align:center; color:#000; border-bottom:3px solid #eee;  padding:20px 0px 5px 0px; font-size:14px;"> Thanks for travelling with us, '.$user_name.'  </td>
			        </tr>
			    </table>
			 </td>
                    </tr>
                    <tr>         
                      <td>                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">                             
                          <tbody>
                            <tr>                                 
                              <td bgcolor="#ffffff" valign="top" style="padding-top:28px;padding-bottom:0;padding-left:0px;padding-right:0px" class="ride-detail">                                                                          
                                <table cellpadding="0" cellspacing="0" border="0" width="49.5%" align="left" class="responsive-table"> 
                                  <tbody>
                                    <tr>                                             
                                      <td style="padding-left:14px;padding-right:14px" class="ride-detail-padding">       
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                          <tbody>
                                            <tr>                                                         
                                              <td align="center" style="color:#333333;font-size:16px;padding-bottom:16px;font-weight:bold">Ride Details</td>
                                            </tr>                                                                                                  
                                            <tr>                                                        
                                              <td style="padding:9px 0 0 0">                              
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">       
                                                  <tbody>
                                                    <tr>                        
                                                      <td valign="top" class="trip-detail" style="border-bottom:1px solid #eeeeee;padding-bottom:10px;padding-left:14px">                                                    
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%" align="left">                                                                             
                                                          <tbody>
                                                            <tr>                                                                                 
                                                              <td align="left" style="width:42px;padding-left:2px" width="42" class="left-space-col">                                                                                 
                                                                <img height="50" style="height:50px" src="http://www.apporiotaxi.com/'.$user_image.'" alt="" class="CToWUd">
                                                              </td>             
                                                              <td align="left" style="padding-left:16px">                                     
                                                                <table>                                  
                                                                  <tbody>
                                                                    <tr>                                 
                                                                      <td style="color:#000000;font-size:16px;line-height:18px">'.$user_name.'</td>                 
                                                                    </tr>                                                      
                                                                  </tbody>
                                                                </table>                  
                                                              </td>                                                                            
                                                            </tr>                                                                         
                                                          </tbody>
                                                        </table>                                                                      
                                                      </td>                                                                 
                                                    </tr>                                                                 
                                                                                                                 
                                                  </tbody>
                                                </table>                                                         
                                              </td>                                                     
                                            </tr>                                                                              
                                            <tr>                                                         
                                              <td style="padding:0 0 0 0">                                   
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                  <tbody>
                                                    <tr>                                                                     
                                                      <td valign="top" class="trip-detail" style="border-bottom:1px solid #eeeeee;padding-bottom:12px;padding-left:14px;padding-top:5px">                                                    
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%" align="left">                                                                             
                                                          <tbody>
                                                            <tr>                                                                                 
                                                              <td align="left" style="width:42px;padding-left:2px" width="42" class="left-space-col">                                    
                                                                <img width="40" style="width:40px" src="http://apporio.co.uk/apporiotaxi/api/mail/minter.png" alt="" class="CToWUd">
                                                              </td>                                                        
                                                              <td align="left" style="color:#000000;font-size:14px;padding-left:16px;padding-top:8px">  '.$distance.' &nbsp;&nbsp;    </td>                                                             
                                                            </tr>                                                                         
                                                          </tbody>
                                                        </table>                                                                      
                                                      </td>                                                                 
                                                    </tr>                                                                                                                              
                                                  </tbody>
                                                </table>                                                         
                                              </td>                                                     
                                            </tr>                                               
                                            <tr>                                                         
                                              <td style="padding:0 0 0 0">                    
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                  <tbody>
                                                    <tr>                                                                     
                                                      <td valign="top" class="trip-detail" style="border-bottom:1px solid #eeeeee;padding-bottom:7px;padding-left:14px;padding-top:5px">                                                    
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%" align="left">                                                                             
                                                          <tbody>
                                                            <tr>                                                                                  
                                                              <td align="left" style="width:42px;padding-left:2px" width="42" class="left-space-col">    
                                                                <img width="38" style="width:38px" src="http://apporio.co.uk/apporiotaxi/api/mail/car.png" alt="" class="CToWUd">
                                                              </td>               
                                                              <td align="left" style="padding-top:4px;color:#000000;font-size:14px;padding-left:16px;line-height:16px">  '.$car_type_name.' </td>                                                                             
                                                            </tr>                                                                         
                                                          </tbody>
                                                        </table>                                                                     
                                                      </td>                                                                 
                                                    </tr>                                                 
                                                  </tbody>
                                                </table>                                                         
                                              </td>                                                     
                                            </tr>                             
                                            <tr style="vertical-align:top;text-align:left;display:block;background-color:#ffffff;padding-bottom:10px;padding-top:5px" align="left" bgcolor="#ffffff">
                                              <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:inline-block;padding:10px 0 0 14px" align="left" valign="top" class="route-address">
                                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:auto;padding:0">                                                                 
                                                  <tbody>                                                                                        
                                                    <tr style="vertical-align:top;text-align:left;width:100%;padding-top:5px" align="left">
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:80px!important;line-height:16px;height:auto;padding:0 0 0 0" align="left" valign="top" class="left-space-address">
                                                        <span style="font-size:14px;font-weight:normal;color:#000000!important">       
                                                          <span class="aBn">                                     
                                                            <a class="aQJ" style="text-decoration:none!important;color:#000000!important">
                                                              <span class="aBn" data-term="goog_354101540">
                                                                <span class="aQJ">'.date('h:i A', strtotime($begin_time)).'
                                                                </span>
                                                              </span>
                                                            </a> 
                                                          </span>                                                                         
                                                        </span>                                                                     
                                                      </td>                                                                     
                                                      <td rowspan="2" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:17px!important;padding:3px 2px 10px 2px" align="left" valign="top">  
                                                        <img width="6" height="84px" src="http://apporio.co.uk/apporiotaxi/api/mail/border.png" style="outline:none;text-decoration:none;float:left;clear:both;display:block;width:6px!important;height:84px;padding-top:5px" align="left" class="CToWUd CToWUd">            
                                                      </td>                                                                     
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:197px;line-height:16px;height:57px;padding:0 10px 10px 0" align="left" valign="top">                                                                         
                                                        <span style="font-size:14px;color:#000000!important;line-height:16px;text-decoration:none">'.$begin_location.' </span>                
                                                      </td>                                                                 
                                                    </tr>              
                                                    <tr style="vertical-align:top;text-align:left;width:100%;padding:0" align="left">
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:80px!important;line-height:16px;height:auto;padding:0 0 0 0" align="left" valign="top" class="left-space-address">
                                                        <span style="font-size:14px;font-weight:normal;color:#000000!important">                             
                                                          <span class="aBn">                                  
                                                            <a class="aQJ" style="text-decoration:none!important;color:#000000!important">
                                                              <span class="aBn" data-term="goog_354101541">
                                                                <span class="aQJ"> '.date('h:i A', strtotime($end_time)).'
                                                                </span>
                                                              </span>
                                                            </a>                                                                             
                                                          </span>                                                                         
                                                        </span>                                     
                                                      </td>                                        
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:197px;line-height:16px;height:auto;padding:0 0px 0 0" align="left" valign="top">                                   
                                                        <span style="font-size:14px;color:#000000!important;line-height:16px;text-decoration:none">'.$end_location.'  </span>                                                                     
                                                      </td>                                                                 
                                                    </tr>                              
                                                  </tbody>                                                             
                                                </table>                                         
                                              </td>                                                     
                                            </tr>                                                 
                                          </tbody>
                                        </table>                                             
                                      </td>                                         
                                    </tr>                                     
                                  </tbody>
                                </table>                                                                          
                                <table cellpadding="0" cellspacing="0" border="0" width="49.5%" align="left" class="responsive-table">
                                  <tbody>
                                    <tr>                                             
                                      <td class="fare-break-up" style="padding-right:14px;padding-left:14px">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                          <tbody>
                                            <tr>                                                         
                                              <td align="center" style="color:#333333;font-size:16px;padding-bottom:16px;font-weight:bold;border-bottom:1px solid #d7d7d7">Bill Details
                                              </td>                                                     
                                            </tr>                                                                                                  
                                            <tr>                                                     
                                              <td bgcolor="#ffffff" class="base-padding">             
                                                <table cellspacing="0" width="100%" style="border-top:2px solid #eee; color:#707070; font-size:14px;">
					                <tr>
					                  <td style="text-align:left; padding:8px;"> Distance Fare for '.$distance.' </td>
					                  <td style="text-align:right; padding:8px;"> Rs. '.$amount.'</td>
					                </tr>
					                <tr style="background:#f3f3f3;">
					                  <td style="text-align:left; padding:8px;">Total Fare </td>
					                  <td style="text-align:right; padding:8px;"> Rs. '.$payment_amount.' </td>
					                </tr>
					                <tr>
					                  <td style="text-align:left; padding:8px; width:100px;"> Payment Method </td>
					                  <td style="text-align:right; padding:8px;"> '.$payment_method.' </td>
					                </tr>
					                <tr style="background:#f3f3f3;">
					                  <th style="text-align:left; padding:10px; color:#000; font-size:16px; width:160px;">Total Bill <span style="color:#707070; font-size:14px; font-weight: normal;">(rounded off)</span> </th>
					                  <th style="text-align:right; padding:10px; color:#000; font-size:17px; "> Rs. '.round($payment_amount).' </th>
					                </tr>
					                <tr>
					                </tr>
					              </table>                                                     
                                              </td>                                                 
                                            </tr>                                                                                                                                   
                                          </tbody>
                                        </table>                                     
                                      </td>                                 
                                    </tr>                             
                                  </tbody>
                                </table>                         
                              </td>                     
                            </tr>                
                          </tbody>
                        </table>             
                      </td>         
                    </tr>
                    <tr>
                    	<td bgcolor="#ffffff">
                           <table width="100%">
			        
			        <tr>
			          <td colspan="2" style="text-align:center; padding:20px 0px 0px 0px; font-size:24px; font-weight: bold;"> Payment  </td>
			        </tr>
			        <tr>
			          <td style="text-align:left;padding:20px;"> Paid by <span class="il"> '.$payment_method.'   </td>
			          <td style="text-align:right;padding:20px;"> Rs. '.round($payment_amount).'  </td>
			        </tr>
			    </table>
			 </td>
                    </tr>
                    
                      
                    
                  </tbody>
                </table> 
                
              </td> 
            </tr> 
          </tbody>
        </table>             
        <p>&nbsp;
          <br>
        </p> 
      </div>  
    </div>
    <br>
  </div>
 </div>';
 mail($user_email, $subject, $message, $headers);
		
	}
	}
	else
	{
	
	
     		$booking_id=$ride_id;
                $rental_ride = $this->Booking_model->rental_ride($booking_id);
                $rental_ride= json_decode(json_encode($rental_ride), true);
            
                if (!empty($rental_ride))
                {
                    $rental_done_ride = $this->Booking_model->rental_done_ride($booking_id);
                    $rental_done_ride= json_decode(json_encode($rental_done_ride), true);
                    
                    if(!empty($rental_done_ride))
                    {
                        $begin_lat = $rental_done_ride['begin_lat'];
                        if (empty($begin_lat))
                        {
                            $begin_lat = $rental_ride['pickup_lat'];
                        }
                        $begin_long = $rental_done_ride['begin_long'];
                        if (empty($begin_long))
                        {
                            $begin_long = $rental_ride['pickup_long'];
                        }
                        $begin_location = $rental_done_ride['begin_location'];
                        if(empty($begin_location))
                        {
                            $begin_location = $rental_ride['pickup_location'];
                        }
                        $end_lat = $rental_done_ride['end_lat'];
                        $end_long = $rental_done_ride['end_long'];
                        $end_location = $rental_done_ride['end_location'];
                        $driver_arive_time = $rental_done_ride['driver_arive_time'];
                        $begin_time = $rental_done_ride['begin_time'];
                        $end_time = $rental_done_ride['end_time'];
                        $total_distance_travel = $rental_done_ride['total_distance_travel'];
                        $total_time_travel = $rental_done_ride['total_time_travel'];
                        $rental_package_price = $rental_done_ride['rental_package_price'];
                        $rental_package_hours = $rental_done_ride['rental_package_hours'];
                        $extra_hours_travel = $rental_done_ride['extra_hours_travel'];
                        $extra_hours_travel_charge = $rental_done_ride['extra_hours_travel_charge'];
                        $rental_package_distance = $rental_done_ride['rental_package_distance'];
                        $extra_distance_travel = $rental_done_ride['extra_distance_travel'];
                        $extra_distance_travel_charge = $rental_done_ride['extra_distance_travel_charge'];
                        $final_bill_amount = $rental_done_ride['final_bill_amount'];
                        $driver_name = $rental_done_ride['driver_name'];
                        $driver_image = $rental_done_ride['driver_image'];
                        $driver_rating = $rental_done_ride['rating'];
                        $driver_phone = $rental_done_ride['driver_phone'];
                        $driver_lat = $rental_done_ride['current_lat'];
                        $driver_long = $rental_done_ride['current_long'];
                        $car_number = $rental_done_ride['car_number'];
                        $driver_location = $rental_done_ride['current_location'];
                        $total_amount = $rental_done_ride['total_amount'];
                        $coupan_price = $rental_done_ride['coupan_price'];
                    }else{
                        $driver_iddd=$rental_ride['driver_id'];
                        $driver_details = $this->Booking_model->get_driver_details($driver_iddd);
                        $driver_details= json_decode(json_encode($driver_details), true);
                        $begin_lat = $rental_ride['pickup_lat'];
                        $begin_long = $rental_ride['pickup_long'];
                        $begin_location = $rental_ride['pickup_location'];
                        $end_lat = "";
                        $end_long = "";
                        $end_location = "";
                        $final_bill_amount = 0;
                        $driver_arive_time = "";
                        $begin_time = "";
                        $end_time = "";
                        $total_distance_travel = "";
                        $total_time_travel = "";
                        $rental_package_price = "";
                        $rental_package_hours = "";
                        $extra_hours_travel = "";
                        $extra_hours_travel_charge = "";
                        $rental_package_distance = "";
                        $extra_distance_travel = "";
                        $extra_distance_travel_charge = "";
                        $driver_name = $driver_details['driver_name']?$driver_details['driver_name']:'';
                        $driver_image = $driver_details['driver_image']?$driver_details['driver_image']:'';
                        $rating  = $driver_details['rating']?$driver_details['rating']:'';
                        $driver_phone = $driver_details['driver_phone']?$driver_details['driver_phone']:'';
                        $driver_lat = $driver_details['current_lat']?$driver_details['current_lat']:'';
                        $driver_long = $driver_details['current_long']?$driver_details['current_long']:'';
                        $car_number = $driver_details['car_number']?$driver_details['car_number']:'';
                        $driver_location = $driver_details['current_location']?$driver_details['current_location']:'';
                        $total_amount = "";
                        $coupan_price = "";
                    }
                    $payment_option_id = $rental_ride->payment_option_id;
                   
                    $payment = $this->Booking_model->payment($payment_option_id);
                    $payment = json_decode(json_encode($payment), true);
                    $payment_option_name = $payment['payment_option_name'];
                    $rental_ride['pickup_lat'] = $begin_lat;
                    $rental_ride['pickup_long'] = $begin_long;
                    $rental_ride['pickup_location'] = $begin_location;
                    $rental_ride['end_lat'] = $end_lat;
                    $rental_ride['end_long'] = $end_long;
                    $rental_ride['end_location'] = $end_location;
                    $rental_ride['final_bill_amount'] = (string)$final_bill_amount;
                    $rental_ride['driver_arive_time'] = $driver_arive_time;
                    $rental_ride['begin_time'] = $begin_time;
                    $rental_ride['end_time'] = $end_time;
                    $rental_ride['total_distance_travel'] = $total_distance_travel;
                    $rental_ride['total_time_travel'] = $total_time_travel;
                    $rental_ride['rental_package_price']  = $rental_package_price;
                    $rental_ride['rental_package_hours'] = $rental_package_hours;
                    $rental_ride['extra_hours_travel'] = $extra_hours_travel;
                    $rental_ride['extra_hours_travel_charge'] = $extra_hours_travel_charge;
                    $rental_ride['rental_package_distance'] = $rental_package_distance;
                    $rental_ride['extra_distance_travel'] = $extra_distance_travel;
                    $rental_ride['extra_distance_travel_charge'] = $extra_distance_travel_charge;
                    $rental_ride['driver_name'] = $driver_name;
                    $rental_ride['driver_image'] = $driver_image;
                    $rental_ride['driver_rating'] = $driver_rating;
                    $rental_ride['driver_phone'] = $driver_phone;
                    $rental_ride['driver_lat'] = $driver_lat;
                    $rental_ride['driver_long'] = $driver_long;
                    $rental_ride['car_number'] = $car_number;
                    $rental_ride['driver_location'] = $driver_location;
                    $rental_ride['total_amount'] = $total_amount;
                    $rental_ride['coupan_price'] = $coupan_price;
                    $rental_ride['payment_option_name'] = $payment_option_name;
                  
                }
                
                
             
          
             
             
             
               $subject = 'Taxi App Payment';
        $from = 'hello@apporio.com';
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: '.$from."\r\n".
            'Reply-To: '.$from."\r\n" .
            'X-Mailer: PHP/' . phpversion();

        $message = '
		<div class="gmail_quote">
  <div dir="ltr">
    <div class="gmail_quote">
      <br>                                   
      <div style="margin:0;padding:0">   
        <table bgcolor="#eeeeee" border="0" cellpadding="0" cellspacing="0" width="100%">     
          <tbody>
             
          </tbody>
        </table>  
        <table bgcolor="#eeeeee" border="0" cellpadding="0" cellspacing="0" width="100%">     
          <tbody>
            <tr>         
              <td bgcolor="#EEEEEE" align="center" style="padding:0 15px 0 15px" class="tracking-space">             
                <table border="0" cellpadding="0" cellspacing="0" style="margin:15px" width="660" class="responsive-table">                 
                  <tbody>
                    <tr>
                    	<td bgcolor="#ffffff">
                           <table width="100%">
			        <tr>
			          <td style="text-align:left;padding:10px;"> '.$rental_ride['user_booking_date_time'].' </td>
			          <td style="text-align:right;padding:10px;"> Taxi App  </td>
			        </tr>
			        <tr>
			          <td colspan="2" style="text-align:center; padding:20px 0px 0px 0px; font-size:46px; font-weight: bold;"> Rs. '.round($rental_ride['total_amount']).'  </td>
			        </tr>
			        <tr>
			          <td colspan="2" style="text-align:center; color:#707070; padding:20px 0px 0px 0px; font-size:14px;"> CRN530048250  </td>
			        </tr>
			        <tr>
			          <td colspan="2" style="text-align:center; color:#000; border-bottom:3px solid #eee;  padding:20px 0px 5px 0px; font-size:14px;"> Thanks for travelling with us, '.$rental_ride['user_name'].'  </td>
			        </tr>
			    </table>
			 </td>
                    </tr>
                    <tr>         
                      <td>                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">                             
                          <tbody>
                            <tr>                                 
                              <td bgcolor="#ffffff" valign="top" style="padding-top:28px;padding-bottom:0;padding-left:0px;padding-right:0px" class="ride-detail">                                                                          
                                <table cellpadding="0" cellspacing="0" border="0" width="49.5%" align="left" class="responsive-table"> 
                                  <tbody>
                                    <tr>                                             
                                      <td style="padding-left:14px;padding-right:14px" class="ride-detail-padding">       
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                          <tbody>
                                            <tr>                                                         
                                              <td align="center" style="color:#333333;font-size:16px;padding-bottom:16px;font-weight:bold">Ride Details</td>
                                            </tr>                                                                                                  
                                            <tr>                                                        
                                              <td style="padding:9px 0 0 0">                              
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">       
                                                  <tbody>
                                                    <tr>                        
                                                      <td valign="top" class="trip-detail" style="border-bottom:1px solid #eeeeee;padding-bottom:10px;padding-left:14px">                                                    
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%" align="left">                                                                             
                                                          <tbody>
                                                            <tr>                                                                                 
                                                              <td align="left" style="width:42px;padding-left:2px" width="42" class="left-space-col">                                                                                 
                                                                <img height="50" style="height:50px" src="http://www.apporiotaxi.com/'.$rental_ride['user_image'].'" alt="" class="CToWUd">
                                                              </td>             
                                                              <td align="left" style="padding-left:16px">                                     
                                                                <table>                                  
                                                                  <tbody>
                                                                    <tr>                                 
                                                                      <td style="color:#000000;font-size:16px;line-height:18px">'.$rental_ride['user_name'].'</td>                 
                                                                    </tr>                                                      
                                                                  </tbody>
                                                                </table>                  
                                                              </td>                                                                            
                                                            </tr>                                                                         
                                                          </tbody>
                                                        </table>                                                                      
                                                      </td>                                                                 
                                                    </tr>                                                                 
                                                                                                                 
                                                  </tbody>
                                                </table>                                                         
                                              </td>                                                     
                                            </tr>                                                                              
                                            <tr>                                                         
                                              <td style="padding:0 0 0 0">                                   
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                  <tbody>
                                                    <tr>                                                                     
                                                      <td valign="top" class="trip-detail" style="border-bottom:1px solid #eeeeee;padding-bottom:12px;padding-left:14px;padding-top:5px">                                                    
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%" align="left">                                                                             
                                                          <tbody>
                                                            <tr>                                                                                 
                                                              <td align="left" style="width:42px;padding-left:2px" width="42" class="left-space-col">                                    
                                                                <img width="40" style="width:40px" src="http://apporio.co.uk/apporiotaxi/api/mail/minter.png" alt="" class="CToWUd">
                                                              </td>                                                        
                                                              <td align="left" style="color:#000000;font-size:14px;padding-left:16px;padding-top:8px">  '.$rental_ride['total_distance_travel'].' &nbsp;&nbsp;    </td>                                                             
                                                            </tr>                                                                         
                                                          </tbody>
                                                        </table>                                                                      
                                                      </td>                                                                 
                                                    </tr>                                                                                                                              
                                                  </tbody>
                                                </table>                                                         
                                              </td>                                                     
                                            </tr>                                               
                                            <tr>                                                         
                                              <td style="padding:0 0 0 0">                    
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                  <tbody>
                                                    <tr>                                                                     
                                                      <td valign="top" class="trip-detail" style="border-bottom:1px solid #eeeeee;padding-bottom:7px;padding-left:14px;padding-top:5px">                                                    
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%" align="left">                                                                             
                                                          <tbody>
                                                            <tr>                                                                                  
                                                              <td align="left" style="width:42px;padding-left:2px" width="42" class="left-space-col">    
                                                                <img width="38" style="width:38px" src="http://apporio.co.uk/apporiotaxi/api/mail/car.png" alt="" class="CToWUd">
                                                              </td>               
                                                              <td align="left" style="padding-top:4px;color:#000000;font-size:14px;padding-left:16px;line-height:16px">  '.$rental_ride['car_type_name'].' </td>                                                                             
                                                            </tr>                                                                         
                                                          </tbody>
                                                        </table>                                                                     
                                                      </td>                                                                 
                                                    </tr>                                                 
                                                  </tbody>
                                                </table>                                                         
                                              </td>                                                     
                                            </tr>                             
                                            <tr style="vertical-align:top;text-align:left;display:block;background-color:#ffffff;padding-bottom:10px;padding-top:5px" align="left" bgcolor="#ffffff">
                                              <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:inline-block;padding:10px 0 0 14px" align="left" valign="top" class="route-address">
                                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:auto;padding:0">                                                                 
                                                  <tbody>                                                                                        
                                                    <tr style="vertical-align:top;text-align:left;width:100%;padding-top:5px" align="left">
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:80px!important;line-height:16px;height:auto;padding:0 0 0 0" align="left" valign="top" class="left-space-address">
                                                        <span style="font-size:14px;font-weight:normal;color:#000000!important">       
                                                          <span class="aBn">                                     
                                                            <a class="aQJ" style="text-decoration:none!important;color:#000000!important">
                                                              <span class="aBn" data-term="goog_354101540">
                                                                <span class="aQJ">'.date('h:i A', strtotime($rental_ride['begin_time'])).'
                                                                </span>
                                                              </span>
                                                            </a> 
                                                          </span>                                                                         
                                                        </span>                                                                     
                                                      </td>                                                                     
                                                      <td rowspan="2" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:17px!important;padding:3px 2px 10px 2px" align="left" valign="top">  
                                                        <img width="6" height="84px" src="http://apporio.co.uk/apporiotaxi/api/mail/border.png" style="outline:none;text-decoration:none;float:left;clear:both;display:block;width:6px!important;height:84px;padding-top:5px" align="left" class="CToWUd CToWUd">            
                                                      </td>                                                                     
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:197px;line-height:16px;height:57px;padding:0 10px 10px 0" align="left" valign="top">                                                                         
                                                        <span style="font-size:14px;color:#000000!important;line-height:16px;text-decoration:none">'.$rental_ride['pickup_location'].' </span>                
                                                      </td>                                                                 
                                                    </tr>              
                                                    <tr style="vertical-align:top;text-align:left;width:100%;padding:0" align="left">
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:80px!important;line-height:16px;height:auto;padding:0 0 0 0" align="left" valign="top" class="left-space-address">
                                                        <span style="font-size:14px;font-weight:normal;color:#000000!important">                             
                                                          <span class="aBn">                                  
                                                            <a class="aQJ" style="text-decoration:none!important;color:#000000!important">
                                                              <span class="aBn" data-term="goog_354101541">
                                                                <span class="aQJ"> '.date('h:i A', strtotime($rental_ride['end_time'])).'
                                                                </span>
                                                              </span>
                                                            </a>                                                                             
                                                          </span>                                                                         
                                                        </span>                                     
                                                      </td>                                        
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:197px;line-height:16px;height:auto;padding:0 0px 0 0" align="left" valign="top">                                   
                                                        <span style="font-size:14px;color:#000000!important;line-height:16px;text-decoration:none">'.$rental_ride['end_location'].'  </span>                                                                     
                                                      </td>                                                                 
                                                    </tr>                              
                                                  </tbody>                                                             
                                                </table>                                         
                                              </td>                                                     
                                            </tr>                                                 
                                          </tbody>
                                        </table>                                             
                                      </td>                                         
                                    </tr>                                     
                                  </tbody>
                                </table>                                                                          
                                <table cellpadding="0" cellspacing="0" border="0" width="49.5%" align="left" class="responsive-table">
                                  <tbody>
                                    <tr>                                             
                                      <td class="fare-break-up" style="padding-right:14px;padding-left:14px">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                          <tbody>
                                            <tr>                                                         
                                              <td align="center" style="color:#333333;font-size:16px;padding-bottom:16px;font-weight:bold;border-bottom:1px solid #d7d7d7">Bill Details
                                              </td>                                                     
                                            </tr>                                                                                                  
                                            <tr>                                                     
                                              <td bgcolor="#ffffff" class="base-padding">             
                                                <table cellspacing="0" width="100%" style="border-top:2px solid #eee; color:#707070; font-size:14px;">
					                <tr>
					                  <td style="text-align:left; padding:8px;"> Distance Fare for '.$rental_ride['total_distance_travel'].' </td>
					                  <td style="text-align:right; padding:8px;"> Rs. '.$rental_ride['rental_package_price'].'</td>
					                </tr>
					                <tr style="background:#f3f3f3;">
					                  <td style="text-align:left; padding:8px;">Total Fare </td>
					                  <td style="text-align:right; padding:8px;"> Rs. '.$rental_ride['total_amount'].' </td>
					                </tr>
					                <tr>
					                  <td style="text-align:left; padding:8px; width:100px;"> Payment Method </td>
					                  <td style="text-align:right; padding:8px;"> '.$rental_ride['payment_option_name'].' </td>
					                </tr>
					                <tr style="background:#f3f3f3;">
					                  <th style="text-align:left; padding:10px; color:#000; font-size:16px; width:160px;">Total Bill <span style="color:#707070; font-size:14px; font-weight: normal;">(rounded off)</span> </th>
					                  <th style="text-align:right; padding:10px; color:#000; font-size:17px; "> Rs. '.round($rental_ride['total_amount']).' </th>
					                </tr>
					                <tr>
					                </tr>
					              </table>                                                     
                                              </td>                                                 
                                            </tr>                                                                                                                                   
                                          </tbody>
                                        </table>                                     
                                      </td>                                 
                                    </tr>                             
                                  </tbody>
                                </table>                         
                              </td>                     
                            </tr>                
                          </tbody>
                        </table>             
                      </td>         
                    </tr>
                    <tr>
                    	<td bgcolor="#ffffff">
                           <table width="100%">
			        
			        <tr>
			          <td colspan="2" style="text-align:center; padding:20px 0px 0px 0px; font-size:24px; font-weight: bold;"> Payment  </td>
			        </tr>
			        <tr>
			          <td style="text-align:left;padding:20px;"> Paid by <span class="il"> '.$rental_ride['payment_option_name'].'   </td>
			          <td style="text-align:right;padding:20px;"> Rs. '.round($rental_ride['total_amount']).'  </td>
			        </tr>
			    </table>
			 </td>
                    </tr>
                    
                      
                    
                  </tbody>
                </table> 
                
              </td> 
            </tr> 
          </tbody>
        </table>             
        <p>&nbsp;
          <br>
        </p> 
      </div>  
    </div>
    <br>
  </div>
 </div>';
 mail($user_email, $subject, $message, $headers);









   
	
	
	}	
		
	}	








public function new_mail()
{
 
$ride_id = $this->input->post('ride_id');
$last_id=$ride_id;
$user_email = $this->input->post('email_id');
$type = $this->input->post('type');
$user_id = $this->session->userdata('user_id'); 

if($type == 1)
{
$list=$this->Booking_model->get_ridedetails($last_id);
	 
    if (!empty($list))
    {
        $user_id = $list['user_id'];
        $car_type_id = $list['car_type_id'];
		$payment_date_time = $list['ride_date'];
        $payment_option_id = $list['payment_option_id'];
        $list1234=$this->Booking_model->paymentmail($payment_option_id);
        $payment_method = $list1234['payment_option_name'];
		$list122=$this->Booking_model->done_rideinfomail($ride_id);
        $distance = $list122['distance'];
        $payment_amount = $list122['total_amount'];
        $begin_time = $list122['begin_time'];
        $begin_location = $list122['begin_location'];
        $end_location = $list122['end_location'];
        $end_time = $list122['end_time'];
        $amount = $list122['amount'];
		$list12=$this->Booking_model->Get_user_details($user_id);
        $user_name = $list12['user_name'];
        $user_image = $list12['user_image'];
		$list121=$this->Booking_model->Get_car_type_mail($car_type_id);
        $car_type_name = $list121['car_type_name'];
		 
        $subject = 'Taxi App Payment';
        $from = 'hello@apporio.com';
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: '.$from."\r\n".
            'Reply-To: '.$from."\r\n" .
            'X-Mailer: PHP/' . phpversion();

        $message = '
		<div class="gmail_quote">
  <div dir="ltr">
    <div class="gmail_quote">
      <br>                                   
      <div style="margin:0;padding:0">   
        <table bgcolor="#eeeeee" border="0" cellpadding="0" cellspacing="0" width="100%">     
          <tbody>
             
          </tbody>
        </table>  
        <table bgcolor="#eeeeee" border="0" cellpadding="0" cellspacing="0" width="100%">     
          <tbody>
            <tr>         
              <td bgcolor="#EEEEEE" align="center" style="padding:0 15px 0 15px" class="tracking-space">             
                <table border="0" cellpadding="0" cellspacing="0" style="margin:15px" width="660" class="responsive-table">                 
                  <tbody>
                    <tr>
                    	<td bgcolor="#ffffff">
                           <table width="100%">
			        <tr>
			          <td style="text-align:left;padding:10px;"> '.$payment_date_time.' </td>
			          <td style="text-align:right;padding:10px;"> Taxi App  </td>
			        </tr>
			        <tr>
			          <td colspan="2" style="text-align:center; padding:20px 0px 0px 0px; font-size:46px; font-weight: bold;"> Rs. '.round($payment_amount).'  </td>
			        </tr>
			        <tr>
			          <td colspan="2" style="text-align:center; color:#707070; padding:20px 0px 0px 0px; font-size:14px;"> CRN530048250  </td>
			        </tr>
			        <tr>
			          <td colspan="2" style="text-align:center; color:#000; border-bottom:3px solid #eee;  padding:20px 0px 5px 0px; font-size:14px;"> Thanks for travelling with us, '.$user_name.'  </td>
			        </tr>
			    </table>
			 </td>
                    </tr>
                    <tr>         
                      <td>                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">                             
                          <tbody>
                            <tr>                                 
                              <td bgcolor="#ffffff" valign="top" style="padding-top:28px;padding-bottom:0;padding-left:0px;padding-right:0px" class="ride-detail">                                                                          
                                <table cellpadding="0" cellspacing="0" border="0" width="49.5%" align="left" class="responsive-table"> 
                                  <tbody>
                                    <tr>                                             
                                      <td style="padding-left:14px;padding-right:14px" class="ride-detail-padding">       
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                          <tbody>
                                            <tr>                                                         
                                              <td align="center" style="color:#333333;font-size:16px;padding-bottom:16px;font-weight:bold">Ride Details</td>
                                            </tr>                                                                                                  
                                            <tr>                                                        
                                              <td style="padding:9px 0 0 0">                              
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">       
                                                  <tbody>
                                                    <tr>                        
                                                      <td valign="top" class="trip-detail" style="border-bottom:1px solid #eeeeee;padding-bottom:10px;padding-left:14px">                                                    
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%" align="left">                                                                             
                                                          <tbody>
                                                            <tr>                                                                                 
                                                              <td align="left" style="width:42px;padding-left:2px" width="42" class="left-space-col">                                                                                 
                                                                <img height="50" style="height:50px" src="http://www.apporiotaxi.com/'.$user_image.'" alt="" class="CToWUd">
                                                              </td>             
                                                              <td align="left" style="padding-left:16px">                                     
                                                                <table>                                  
                                                                  <tbody>
                                                                    <tr>                                 
                                                                      <td style="color:#000000;font-size:16px;line-height:18px">'.$user_name.'</td>                 
                                                                    </tr>                                                      
                                                                  </tbody>
                                                                </table>                  
                                                              </td>                                                                            
                                                            </tr>                                                                         
                                                          </tbody>
                                                        </table>                                                                      
                                                      </td>                                                                 
                                                    </tr>                                                                 
                                                                                                                 
                                                  </tbody>
                                                </table>                                                         
                                              </td>                                                     
                                            </tr>                                                                              
                                            <tr>                                                         
                                              <td style="padding:0 0 0 0">                                   
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                  <tbody>
                                                    <tr>                                                                     
                                                      <td valign="top" class="trip-detail" style="border-bottom:1px solid #eeeeee;padding-bottom:12px;padding-left:14px;padding-top:5px">                                                    
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%" align="left">                                                                             
                                                          <tbody>
                                                            <tr>                                                                                 
                                                              <td align="left" style="width:42px;padding-left:2px" width="42" class="left-space-col">                                    
                                                                <img width="40" style="width:40px" src="http://apporio.co.uk/apporiotaxi/api/mail/minter.png" alt="" class="CToWUd">
                                                              </td>                                                        
                                                              <td align="left" style="color:#000000;font-size:14px;padding-left:16px;padding-top:8px">  '.$distance.' &nbsp;&nbsp;    </td>                                                             
                                                            </tr>                                                                         
                                                          </tbody>
                                                        </table>                                                                      
                                                      </td>                                                                 
                                                    </tr>                                                                                                                              
                                                  </tbody>
                                                </table>                                                         
                                              </td>                                                     
                                            </tr>                                               
                                            <tr>                                                         
                                              <td style="padding:0 0 0 0">                    
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                  <tbody>
                                                    <tr>                                                                     
                                                      <td valign="top" class="trip-detail" style="border-bottom:1px solid #eeeeee;padding-bottom:7px;padding-left:14px;padding-top:5px">                                                    
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%" align="left">                                                                             
                                                          <tbody>
                                                            <tr>                                                                                  
                                                              <td align="left" style="width:42px;padding-left:2px" width="42" class="left-space-col">    
                                                                <img width="38" style="width:38px" src="http://apporio.co.uk/apporiotaxi/api/mail/car.png" alt="" class="CToWUd">
                                                              </td>               
                                                              <td align="left" style="padding-top:4px;color:#000000;font-size:14px;padding-left:16px;line-height:16px">  '.$car_type_name.' </td>                                                                             
                                                            </tr>                                                                         
                                                          </tbody>
                                                        </table>                                                                     
                                                      </td>                                                                 
                                                    </tr>                                                 
                                                  </tbody>
                                                </table>                                                         
                                              </td>                                                     
                                            </tr>                             
                                            <tr style="vertical-align:top;text-align:left;display:block;background-color:#ffffff;padding-bottom:10px;padding-top:5px" align="left" bgcolor="#ffffff">
                                              <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:inline-block;padding:10px 0 0 14px" align="left" valign="top" class="route-address">
                                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:auto;padding:0">                                                                 
                                                  <tbody>                                                                                        
                                                    <tr style="vertical-align:top;text-align:left;width:100%;padding-top:5px" align="left">
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:80px!important;line-height:16px;height:auto;padding:0 0 0 0" align="left" valign="top" class="left-space-address">
                                                        <span style="font-size:14px;font-weight:normal;color:#000000!important">       
                                                          <span class="aBn">                                     
                                                            <a class="aQJ" style="text-decoration:none!important;color:#000000!important">
                                                              <span class="aBn" data-term="goog_354101540">
                                                                <span class="aQJ">'.date('h:i A', strtotime($begin_time)).'
                                                                </span>
                                                              </span>
                                                            </a> 
                                                          </span>                                                                         
                                                        </span>                                                                     
                                                      </td>                                                                     
                                                      <td rowspan="2" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:17px!important;padding:3px 2px 10px 2px" align="left" valign="top">  
                                                        <img width="6" height="84px" src="http://apporio.co.uk/apporiotaxi/api/mail/border.png" style="outline:none;text-decoration:none;float:left;clear:both;display:block;width:6px!important;height:84px;padding-top:5px" align="left" class="CToWUd CToWUd">            
                                                      </td>                                                                     
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:197px;line-height:16px;height:57px;padding:0 10px 10px 0" align="left" valign="top">                                                                         
                                                        <span style="font-size:14px;color:#000000!important;line-height:16px;text-decoration:none">'.$begin_location.' </span>                
                                                      </td>                                                                 
                                                    </tr>              
                                                    <tr style="vertical-align:top;text-align:left;width:100%;padding:0" align="left">
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:80px!important;line-height:16px;height:auto;padding:0 0 0 0" align="left" valign="top" class="left-space-address">
                                                        <span style="font-size:14px;font-weight:normal;color:#000000!important">                             
                                                          <span class="aBn">                                  
                                                            <a class="aQJ" style="text-decoration:none!important;color:#000000!important">
                                                              <span class="aBn" data-term="goog_354101541">
                                                                <span class="aQJ"> '.date('h:i A', strtotime($end_time)).'
                                                                </span>
                                                              </span>
                                                            </a>                                                                             
                                                          </span>                                                                         
                                                        </span>                                     
                                                      </td>                                        
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:197px;line-height:16px;height:auto;padding:0 0px 0 0" align="left" valign="top">                                   
                                                        <span style="font-size:14px;color:#000000!important;line-height:16px;text-decoration:none">'.$end_location.'  </span>                                                                     
                                                      </td>                                                                 
                                                    </tr>                              
                                                  </tbody>                                                             
                                                </table>                                         
                                              </td>                                                     
                                            </tr>                                                 
                                          </tbody>
                                        </table>                                             
                                      </td>                                         
                                    </tr>                                     
                                  </tbody>
                                </table>                                                                          
                                <table cellpadding="0" cellspacing="0" border="0" width="49.5%" align="left" class="responsive-table">
                                  <tbody>
                                    <tr>                                             
                                      <td class="fare-break-up" style="padding-right:14px;padding-left:14px">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                          <tbody>
                                            <tr>                                                         
                                              <td align="center" style="color:#333333;font-size:16px;padding-bottom:16px;font-weight:bold;border-bottom:1px solid #d7d7d7">Bill Details
                                              </td>                                                     
                                            </tr>                                                                                                  
                                            <tr>                                                     
                                              <td bgcolor="#ffffff" class="base-padding">             
                                                <table cellspacing="0" width="100%" style="border-top:2px solid #eee; color:#707070; font-size:14px;">
					                <tr>
					                  <td style="text-align:left; padding:8px;"> Distance Fare for '.$distance.' </td>
					                  <td style="text-align:right; padding:8px;"> Rs. '.$amount.'</td>
					                </tr>
					                <tr style="background:#f3f3f3;">
					                  <td style="text-align:left; padding:8px;">Total Fare </td>
					                  <td style="text-align:right; padding:8px;"> Rs. '.$payment_amount.' </td>
					                </tr>
					                <tr>
					                  <td style="text-align:left; padding:8px; width:100px;"> Payment Method </td>
					                  <td style="text-align:right; padding:8px;"> '.$payment_method.' </td>
					                </tr>
					                <tr style="background:#f3f3f3;">
					                  <th style="text-align:left; padding:10px; color:#000; font-size:16px; width:160px;">Total Bill <span style="color:#707070; font-size:14px; font-weight: normal;">(rounded off)</span> </th>
					                  <th style="text-align:right; padding:10px; color:#000; font-size:17px; "> Rs. '.round($payment_amount).' </th>
					                </tr>
					                <tr>
					                </tr>
					              </table>                                                     
                                              </td>                                                 
                                            </tr>                                                                                                                                   
                                          </tbody>
                                        </table>                                     
                                      </td>                                 
                                    </tr>                             
                                  </tbody>
                                </table>                         
                              </td>                     
                            </tr>                
                          </tbody>
                        </table>             
                      </td>         
                    </tr>
                    <tr>
                    	<td bgcolor="#ffffff">
                           <table width="100%">
			        
			        <tr>
			          <td colspan="2" style="text-align:center; padding:20px 0px 0px 0px; font-size:24px; font-weight: bold;"> Payment  </td>
			        </tr>
			        <tr>
			          <td style="text-align:left;padding:20px;"> Paid by <span class="il"> '.$payment_method.'   </td>
			          <td style="text-align:right;padding:20px;"> Rs. '.round($payment_amount).'  </td>
			        </tr>
			    </table>
			 </td>
                    </tr>
                    
                      
                    
                  </tbody>
                </table> 
                
              </td> 
            </tr> 
          </tbody>
        </table>             
        <p>&nbsp;
          <br>
        </p> 
      </div>  
    </div>
    <br>
  </div>
 </div>';
 mail($user_email, $subject, $message, $headers);
		
		
		
		
		
		
	}	
}
else
{

     		$booking_id=$this->input->post('ride_id');
		$rental_ride = $this->Booking_model->rental_ride($booking_id);
                $rental_ride= json_decode(json_encode($rental_ride), true);
                //  echo $booking_id;die();
                if (!empty($rental_ride))
                {
                    $rental_done_ride = $this->Booking_model->rental_done_ride($booking_id);
                    $rental_done_ride= json_decode(json_encode($rental_done_ride), true);
                    
                    if(!empty($rental_done_ride))
                    {
                        $begin_lat = $rental_done_ride['begin_lat'];
                        if (empty($begin_lat))
                        {
                            $begin_lat = $rental_ride['pickup_lat'];
                        }
                        $begin_long = $rental_done_ride['begin_long'];
                        if (empty($begin_long))
                        {
                            $begin_long = $rental_ride['pickup_long'];
                        }
                        $begin_location = $rental_done_ride['begin_location'];
                        if(empty($begin_location))
                        {
                            $begin_location = $rental_ride['pickup_location'];
                        }
                        $end_lat = $rental_done_ride['end_lat'];
                        $end_long = $rental_done_ride['end_long'];
                        $end_location = $rental_done_ride['end_location'];
                        $driver_arive_time = $rental_done_ride['driver_arive_time'];
                        $begin_time = $rental_done_ride['begin_time'];
                        $end_time = $rental_done_ride['end_time'];
                        $total_distance_travel = $rental_done_ride['total_distance_travel'];
                        $total_time_travel = $rental_done_ride['total_time_travel'];
                        $rental_package_price = $rental_done_ride['rental_package_price'];
                        $rental_package_hours = $rental_done_ride['rental_package_hours'];
                        $extra_hours_travel = $rental_done_ride['extra_hours_travel'];
                        $extra_hours_travel_charge = $rental_done_ride['extra_hours_travel_charge'];
                        $rental_package_distance = $rental_done_ride['rental_package_distance'];
                        $extra_distance_travel = $rental_done_ride['extra_distance_travel'];
                        $extra_distance_travel_charge = $rental_done_ride['extra_distance_travel_charge'];
                        $final_bill_amount = $rental_done_ride['final_bill_amount'];
                        $driver_name = $rental_done_ride['driver_name'];
                        $driver_image = $rental_done_ride['driver_image'];
                        $driver_rating = $rental_done_ride['rating'];
                        $driver_phone = $rental_done_ride['driver_phone'];
                        $driver_lat = $rental_done_ride['current_lat'];
                        $driver_long = $rental_done_ride['current_long'];
                        $car_number = $rental_done_ride['car_number'];
                        $driver_location = $rental_done_ride['current_location'];
                        $total_amount = $rental_done_ride['total_amount'];
                        $coupan_price = $rental_done_ride['coupan_price'];
                    }else{
                        $driver_iddd=$rental_ride['driver_id'];
                        $driver_details = $this->Booking_model->get_driver_details($driver_iddd);
                        $driver_details= json_decode(json_encode($driver_details), true);
                        $begin_lat = $rental_ride['pickup_lat'];
                        $begin_long = $rental_ride['pickup_long'];
                        $begin_location = $rental_ride['pickup_location'];
                        $end_lat = "";
                        $end_long = "";
                        $end_location = "";
                        $final_bill_amount = 0;
                        $driver_arive_time = "";
                        $begin_time = "";
                        $end_time = "";
                        $total_distance_travel = "";
                        $total_time_travel = "";
                        $rental_package_price = "";
                        $rental_package_hours = "";
                        $extra_hours_travel = "";
                        $extra_hours_travel_charge = "";
                        $rental_package_distance = "";
                        $extra_distance_travel = "";
                        $extra_distance_travel_charge = "";
                        $driver_name = $driver_details['driver_name']?$driver_details['driver_name']:'';
                        $driver_image = $driver_details['driver_image']?$driver_details['driver_image']:'';
                        $rating  = $driver_details['rating']?$driver_details['rating']:'';
                        $driver_phone = $driver_details['driver_phone']?$driver_details['driver_phone']:'';
                        $driver_lat = $driver_details['current_lat']?$driver_details['current_lat']:'';
                        $driver_long = $driver_details['current_long']?$driver_details['current_long']:'';
                        $car_number = $driver_details['car_number']?$driver_details['car_number']:'';
                        $driver_location = $driver_details['current_location']?$driver_details['current_location']:'';
                        $total_amount = "";
                        $coupan_price = "";
                    }
                    $payment_option_id = $rental_ride->payment_option_id;
                   
                    $payment = $this->Booking_model->payment($payment_option_id);
                    $payment = json_decode(json_encode($payment), true);
                    $payment_option_name = $payment['payment_option_name'];
                    $rental_ride['pickup_lat'] = $begin_lat;
                    $rental_ride['pickup_long'] = $begin_long;
                    $rental_ride['pickup_location'] = $begin_location;
                    $rental_ride['end_lat'] = $end_lat;
                    $rental_ride['end_long'] = $end_long;
                    $rental_ride['end_location'] = $end_location;
                    $rental_ride['final_bill_amount'] = (string)$final_bill_amount;
                    $rental_ride['driver_arive_time'] = $driver_arive_time;
                    $rental_ride['begin_time'] = $begin_time;
                    $rental_ride['end_time'] = $end_time;
                    $rental_ride['total_distance_travel'] = $total_distance_travel;
                    $rental_ride['total_time_travel'] = $total_time_travel;
                    $rental_ride['rental_package_price']  = $rental_package_price;
                    $rental_ride['rental_package_hours'] = $rental_package_hours;
                    $rental_ride['extra_hours_travel'] = $extra_hours_travel;
                    $rental_ride['extra_hours_travel_charge'] = $extra_hours_travel_charge;
                    $rental_ride['rental_package_distance'] = $rental_package_distance;
                    $rental_ride['extra_distance_travel'] = $extra_distance_travel;
                    $rental_ride['extra_distance_travel_charge'] = $extra_distance_travel_charge;
                    $rental_ride['driver_name'] = $driver_name;
                    $rental_ride['driver_image'] = $driver_image;
                    $rental_ride['driver_rating'] = $driver_rating;
                    $rental_ride['driver_phone'] = $driver_phone;
                    $rental_ride['driver_lat'] = $driver_lat;
                    $rental_ride['driver_long'] = $driver_long;
                    $rental_ride['car_number'] = $car_number;
                    $rental_ride['driver_location'] = $driver_location;
                    $rental_ride['total_amount'] = $total_amount;
                    $rental_ride['coupan_price'] = $coupan_price;
                    $rental_ride['payment_option_name'] = $payment_option_name;
                  
                }



        $subject = 'Taxi App Payment';
        $from = 'hello@apporio.com';
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: '.$from."\r\n".
            'Reply-To: '.$from."\r\n" .
            'X-Mailer: PHP/' . phpversion();

        $message = '
		<div class="gmail_quote">
  <div dir="ltr">
    <div class="gmail_quote">
      <br>                                   
      <div style="margin:0;padding:0">   
        <table bgcolor="#eeeeee" border="0" cellpadding="0" cellspacing="0" width="100%">     
          <tbody>
             
          </tbody>
        </table>  
        <table bgcolor="#eeeeee" border="0" cellpadding="0" cellspacing="0" width="100%">     
          <tbody>
            <tr>         
              <td bgcolor="#EEEEEE" align="center" style="padding:0 15px 0 15px" class="tracking-space">             
                <table border="0" cellpadding="0" cellspacing="0" style="margin:15px" width="660" class="responsive-table">                 
                  <tbody>
                    <tr>
                    	<td bgcolor="#ffffff">
                           <table width="100%">
			        <tr>
			          <td style="text-align:left;padding:10px;"> '.$rental_ride['user_booking_date_time'].' </td>
			          <td style="text-align:right;padding:10px;"> Taxi App  </td>
			        </tr>
			        <tr>
			          <td colspan="2" style="text-align:center; padding:20px 0px 0px 0px; font-size:46px; font-weight: bold;"> Rs. '.round($rental_ride['total_amount']).'  </td>
			        </tr>
			        <tr>
			          <td colspan="2" style="text-align:center; color:#707070; padding:20px 0px 0px 0px; font-size:14px;"> CRN530048250  </td>
			        </tr>
			        <tr>
			          <td colspan="2" style="text-align:center; color:#000; border-bottom:3px solid #eee;  padding:20px 0px 5px 0px; font-size:14px;"> Thanks for travelling with us, '.$rental_ride['user_name'].'  </td>
			        </tr>
			    </table>
			 </td>
                    </tr>
                    <tr>         
                      <td>                        
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">                             
                          <tbody>
                            <tr>                                 
                              <td bgcolor="#ffffff" valign="top" style="padding-top:28px;padding-bottom:0;padding-left:0px;padding-right:0px" class="ride-detail">                                                                          
                                <table cellpadding="0" cellspacing="0" border="0" width="49.5%" align="left" class="responsive-table"> 
                                  <tbody>
                                    <tr>                                             
                                      <td style="padding-left:14px;padding-right:14px" class="ride-detail-padding">       
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                          <tbody>
                                            <tr>                                                         
                                              <td align="center" style="color:#333333;font-size:16px;padding-bottom:16px;font-weight:bold">Ride Details</td>
                                            </tr>                                                                                                  
                                            <tr>                                                        
                                              <td style="padding:9px 0 0 0">                              
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">       
                                                  <tbody>
                                                    <tr>                        
                                                      <td valign="top" class="trip-detail" style="border-bottom:1px solid #eeeeee;padding-bottom:10px;padding-left:14px">                                                    
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%" align="left">                                                                             
                                                          <tbody>
                                                            <tr>                                                                                 
                                                              <td align="left" style="width:42px;padding-left:2px" width="42" class="left-space-col">                                                                                 
                                                                <img height="50" style="height:50px" src="http://www.apporiotaxi.com/'.$rental_ride['user_image'].'" alt="" class="CToWUd">
                                                              </td>             
                                                              <td align="left" style="padding-left:16px">                                     
                                                                <table>                                  
                                                                  <tbody>
                                                                    <tr>                                 
                                                                      <td style="color:#000000;font-size:16px;line-height:18px">'.$rental_ride['user_name'].'</td>                 
                                                                    </tr>                                                      
                                                                  </tbody>
                                                                </table>                  
                                                              </td>                                                                            
                                                            </tr>                                                                         
                                                          </tbody>
                                                        </table>                                                                      
                                                      </td>                                                                 
                                                    </tr>                                                                 
                                                                                                                 
                                                  </tbody>
                                                </table>                                                         
                                              </td>                                                     
                                            </tr>                                                                              
                                            <tr>                                                         
                                              <td style="padding:0 0 0 0">                                   
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                  <tbody>
                                                    <tr>                                                                     
                                                      <td valign="top" class="trip-detail" style="border-bottom:1px solid #eeeeee;padding-bottom:12px;padding-left:14px;padding-top:5px">                                                    
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%" align="left">                                                                             
                                                          <tbody>
                                                            <tr>                                                                                 
                                                              <td align="left" style="width:42px;padding-left:2px" width="42" class="left-space-col">                                    
                                                                <img width="40" style="width:40px" src="http://apporio.co.uk/apporiotaxi/api/mail/minter.png" alt="" class="CToWUd">
                                                              </td>                                                        
                                                              <td align="left" style="color:#000000;font-size:14px;padding-left:16px;padding-top:8px">  '.$rental_ride['total_distance_travel'].' &nbsp;&nbsp;    </td>                                                             
                                                            </tr>                                                                         
                                                          </tbody>
                                                        </table>                                                                      
                                                      </td>                                                                 
                                                    </tr>                                                                                                                              
                                                  </tbody>
                                                </table>                                                         
                                              </td>                                                     
                                            </tr>                                               
                                            <tr>                                                         
                                              <td style="padding:0 0 0 0">                    
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                  <tbody>
                                                    <tr>                                                                     
                                                      <td valign="top" class="trip-detail" style="border-bottom:1px solid #eeeeee;padding-bottom:7px;padding-left:14px;padding-top:5px">                                                    
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%" align="left">                                                                             
                                                          <tbody>
                                                            <tr>                                                                                  
                                                              <td align="left" style="width:42px;padding-left:2px" width="42" class="left-space-col">    
                                                                <img width="38" style="width:38px" src="http://apporio.co.uk/apporiotaxi/api/mail/car.png" alt="" class="CToWUd">
                                                              </td>               
                                                              <td align="left" style="padding-top:4px;color:#000000;font-size:14px;padding-left:16px;line-height:16px">  '.$rental_ride['car_type_name'].' </td>                                                                             
                                                            </tr>                                                                         
                                                          </tbody>
                                                        </table>                                                                     
                                                      </td>                                                                 
                                                    </tr>                                                 
                                                  </tbody>
                                                </table>                                                         
                                              </td>                                                     
                                            </tr>                             
                                            <tr style="vertical-align:top;text-align:left;display:block;background-color:#ffffff;padding-bottom:10px;padding-top:5px" align="left" bgcolor="#ffffff">
                                              <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:inline-block;padding:10px 0 0 14px" align="left" valign="top" class="route-address">
                                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:auto;padding:0">                                                                 
                                                  <tbody>                                                                                        
                                                    <tr style="vertical-align:top;text-align:left;width:100%;padding-top:5px" align="left">
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:80px!important;line-height:16px;height:auto;padding:0 0 0 0" align="left" valign="top" class="left-space-address">
                                                        <span style="font-size:14px;font-weight:normal;color:#000000!important">       
                                                          <span class="aBn">                                     
                                                            <a class="aQJ" style="text-decoration:none!important;color:#000000!important">
                                                              <span class="aBn" data-term="goog_354101540">
                                                                <span class="aQJ">'.date('h:i A', strtotime($rental_ride['begin_time'])).'
                                                                </span>
                                                              </span>
                                                            </a> 
                                                          </span>                                                                         
                                                        </span>                                                                     
                                                      </td>                                                                     
                                                      <td rowspan="2" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:17px!important;padding:3px 2px 10px 2px" align="left" valign="top">  
                                                        <img width="6" height="84px" src="http://apporio.co.uk/apporiotaxi/api/mail/border.png" style="outline:none;text-decoration:none;float:left;clear:both;display:block;width:6px!important;height:84px;padding-top:5px" align="left" class="CToWUd CToWUd">            
                                                      </td>                                                                     
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:197px;line-height:16px;height:57px;padding:0 10px 10px 0" align="left" valign="top">                                                                         
                                                        <span style="font-size:14px;color:#000000!important;line-height:16px;text-decoration:none">'.$rental_ride['pickup_location'].' </span>                
                                                      </td>                                                                 
                                                    </tr>              
                                                    <tr style="vertical-align:top;text-align:left;width:100%;padding:0" align="left">
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:80px!important;line-height:16px;height:auto;padding:0 0 0 0" align="left" valign="top" class="left-space-address">
                                                        <span style="font-size:14px;font-weight:normal;color:#000000!important">                             
                                                          <span class="aBn">                                  
                                                            <a class="aQJ" style="text-decoration:none!important;color:#000000!important">
                                                              <span class="aBn" data-term="goog_354101541">
                                                                <span class="aQJ"> '.date('h:i A', strtotime($rental_ride['end_time'])).'
                                                                </span>
                                                              </span>
                                                            </a>                                                                             
                                                          </span>                                                                         
                                                        </span>                                     
                                                      </td>                                        
                                                      <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;display:table-cell;width:197px;line-height:16px;height:auto;padding:0 0px 0 0" align="left" valign="top">                                   
                                                        <span style="font-size:14px;color:#000000!important;line-height:16px;text-decoration:none">'.$rental_ride['end_location'].'  </span>                                                                     
                                                      </td>                                                                 
                                                    </tr>                              
                                                  </tbody>                                                             
                                                </table>                                         
                                              </td>                                                     
                                            </tr>                                                 
                                          </tbody>
                                        </table>                                             
                                      </td>                                         
                                    </tr>                                     
                                  </tbody>
                                </table>                                                                          
                                <table cellpadding="0" cellspacing="0" border="0" width="49.5%" align="left" class="responsive-table">
                                  <tbody>
                                    <tr>                                             
                                      <td class="fare-break-up" style="padding-right:14px;padding-left:14px">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                          <tbody>
                                            <tr>                                                         
                                              <td align="center" style="color:#333333;font-size:16px;padding-bottom:16px;font-weight:bold;border-bottom:1px solid #d7d7d7">Bill Details
                                              </td>                                                     
                                            </tr>                                                                                                  
                                            <tr>                                                     
                                              <td bgcolor="#ffffff" class="base-padding">             
                                                <table cellspacing="0" width="100%" style="border-top:2px solid #eee; color:#707070; font-size:14px;">
					                <tr>
					                  <td style="text-align:left; padding:8px;"> Distance Fare for '.$rental_ride['total_distance_travel'].' </td>
					                  <td style="text-align:right; padding:8px;"> Rs. '.$rental_ride['rental_package_price'].'</td>
					                </tr>
					                <tr style="background:#f3f3f3;">
					                  <td style="text-align:left; padding:8px;">Total Fare </td>
					                  <td style="text-align:right; padding:8px;"> Rs. '.$rental_ride['total_amount'].' </td>
					                </tr>
					                <tr>
					                  <td style="text-align:left; padding:8px; width:100px;"> Payment Method </td>
					                  <td style="text-align:right; padding:8px;"> '.$rental_ride['payment_option_name'].' </td>
					                </tr>
					                <tr style="background:#f3f3f3;">
					                  <th style="text-align:left; padding:10px; color:#000; font-size:16px; width:160px;">Total Bill <span style="color:#707070; font-size:14px; font-weight: normal;">(rounded off)</span> </th>
					                  <th style="text-align:right; padding:10px; color:#000; font-size:17px; "> Rs. '.round($rental_ride['total_amount']).' </th>
					                </tr>
					                <tr>
					                </tr>
					              </table>                                                     
                                              </td>                                                 
                                            </tr>                                                                                                                                   
                                          </tbody>
                                        </table>                                     
                                      </td>                                 
                                    </tr>                             
                                  </tbody>
                                </table>                         
                              </td>                     
                            </tr>                
                          </tbody>
                        </table>             
                      </td>         
                    </tr>
                    <tr>
                    	<td bgcolor="#ffffff">
                           <table width="100%">
			        
			        <tr>
			          <td colspan="2" style="text-align:center; padding:20px 0px 0px 0px; font-size:24px; font-weight: bold;"> Payment  </td>
			        </tr>
			        <tr>
			          <td style="text-align:left;padding:20px;"> Paid by <span class="il"> '.$rental_ride['payment_option_name'].'   </td>
			          <td style="text-align:right;padding:20px;"> Rs. '.round($rental_ride['total_amount']).'  </td>
			        </tr>
			    </table>
			 </td>
                    </tr>
                    
                      
                    
                  </tbody>
                </table> 
                
              </td> 
            </tr> 
          </tbody>
        </table>             
        <p>&nbsp;
          <br>
        </p> 
      </div>  
    </div>
    <br>
  </div>
 </div>';
 mail($user_email, $subject, $message, $headers);










}

}
}
?>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		