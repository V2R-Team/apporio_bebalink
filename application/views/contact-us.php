 <?php if($this->session->flashdata('Rider_success')):?>
     <script>alert("We Will Revert You Soon....!!");</script>
<?php endif; ?>
<?php include('header.php'); ?>
<script>
function validateForm() {
    var x = document.forms["myForm"]["uname"].value;
	var y = document.forms["myForm"]["email"].value;
	var z = document.forms["myForm"]["skypephone"].value;
	var a = document.forms["myForm"]["subject"].value;
	 
	 
    if (x == "") {
        alert("Please Enter Name");
        return false;
    }
	 else if (y == "") {
        alert("Please Enter Email");
        return false;
    }
	else if (z == "") {
        alert("Please Enter Skype Phone");
        return false;
    }
	else if (a == "") {
        alert("Please Enter Message");
        return false;
    }

}
</script>

<div class="inner-banner">
<img src="<?php echo base_url();?>/images/contact_banner.jpg" alt="">
 
<div class="inner-banner-holder">
<div class="container">
<h2><?php echo $about[0]['title']?></h2>
 
</div>
</div> 
</div>
<div class="clear"></div>
 
<div id="main-content">
 
<section class="pb-50">
<div class="row">
<div class="col-md-6" style="
-webkit-filter: grayscale(100%);
filter: grayscale(100%);">
<!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2016220.04997142!2d7.986606640953556!3d9.250571776855407!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x104e0baf7da48d0d%3A0x99a8fe4168c50bc8!2sNigeria!5e0!3m2!1sen!2sin!4v1507126008655" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>-->
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2428912.965716737!2d37.003343044937616!3d1.0185473011134731!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182780d08350900f%3A0x403b0eb0a1976dd9!2sKenya!5e0!3m2!1sen!2sin!4v1511852089646" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>

 </div>
    <div class="col-md-6">
         <form name="myForm" action="<?php echo base_url();?>index.php/Welcome/Contact_us_form" onsubmit="return validateForm()" method="post">
            <h2 class="section-title mb-40"><?php echo $about[0]['title1']?></h2>
                  <div class="form-group edit_profile_label">
                    <input type="text" name="uname" class="form-control edit_profile_field" id="" placeholder="Your Name" >
                  </div>
            <div class="form-group edit_profile_label">
                    <input type="email" name="email" class="form-control edit_profile_field" id="" placeholder="Your Email">
                  </div>
            <div class="form-group edit_profile_label">
                    <input type="text" name="skypephone" class="form-control edit_profile_field" id="" placeholder="Skype/Phone" >
                  </div>
            <div class="form-group edit_profile_label">
                    <input type="text" name="subject" class="form-control edit_profile_field" id="" placeholder="Your Subject" >
                  </div>
            
            <div class="form-group edit_profile_label">
                    <input type="submit" name="submit" class="btn btn-info mt-10 pt-10 pb-10" id="" value="Send Message">
                  </div>
        </form>    
    </div>
    
</div>
</section> 
</div> 
     <div class="clear"></div>
<div class="contact_foot pb-50">
 
<div class="container">
              
                <div class="col-md-1"></div>
                <div class="col-md-4 col-sm-6">
                  <div class="contact-cont">
                    <div class="contact-icon">
                      <div class="icon_contact"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                    </div>
                    <div class="contact-text">
                      <h3><span class="bold">EMAIL</span></h3>
                      <p><a href="mailto:<?php echo $about[0]['email']?>"><?php echo $about[0]['email']?></a></p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6">
                  <div class="contact-cont">
                    <div class="contact-icon">
                      <div class="icon_contact"><i class="fa fa-phone" aria-hidden="true"></i></div>
                    </div>
                    <div class="contact-text">
                      <h3><span class="bold">CALL US</span></h3>
                      <p><?php echo $about[0]['phone']?></p>
                    </div>
                  </div>
                </div>
    <div class="col-md-3 col-sm-6">
                  <div class="contact-cont">
                    <div class="contact-icon">
                      <div class="icon_contact"><i class="fa fa-skype" aria-hidden="true"></i></div>
                    </div>
                    <div class="contact-text">
                      <h3><span class="bold">SKYPE</span></h3>
                      <p><?php echo $about[0]['skype']?></p>
                    </div>
                  </div>
                </div>
                
 
</div> 
</div>



 

<?php include('footer.php'); ?>