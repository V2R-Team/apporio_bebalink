 
<!DOCTYPE html>
<html>
<title><?php echo $header_data['web_title'];?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo base_url();?>webstatic/css/style1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/custom1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.min.css" type="text/css">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
function getId(val) {
    $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Booking_controller/car_names",
        data: "city_id="+val,
        success: 
        function(data){
       $('#car_type_id').html(data);
        }
    });
}
function getId1(val) {
  var city_id=document.getElementById("city_id").value;
   var car_id =val;
    $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Booking_controller/car_ratecards",
       data: {
        car_type_id : val,
        city_id : city_id,
        }  ,
        success: 
        function(data){
           $("#result").html(data);
        }
    });
}
</script> 

<style>


.rgt_side{ }



body {
    position: relative;
    overflow-x: hidden;
}
body,
html { height: 100%;}
.nav .open > a,
.nav .open > a:hover,
.nav .open > a:focus {background-color: transparent;}

/*-------------------------------*/
/*           Wrappers            */
/*-------------------------------*/


.clear{clear:both !important;}


</style>

<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
}

th, td {
    border: none;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

</style>


<body class="main_bg">
<!-- Sidebar -->
<!-- Page Content -->
<div class="main_dv">
 <div class="lft_side">
     <div id="wrapper1">
         <div class="overlay1"></div>

         <!-- Sidebar -->
         <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper1" role="navigation">
             <ul class="nav sidebar-nav">

                 <li class="sidebar-brand">
                     <a href="<?php echo base_url();?>">
                         <img src="<?php echo base_url();?>images/logo.png" alt="">
                     </a>
                 </li>

                 <li>
                     <a href="<?php echo base_url();?>">Home</a>
                 </li>
                 <li>
                     <a href="<?php echo base_url();?>index.php/Booking_controller/search_cab_city">Book Your Ride</a>
                 </li>
                  <?php if($this->session->userdata('user') != ""){?>
                  <li>
                   <a href="<?php echo base_url();?>index.php/Booking_controller/user_allrides">Your rides</a>
    		  </li>
   		 <?php } ?>
                 <li>
                     <a href="<?php echo base_url();?>index.php/Booking_controller/view_ratecard">Rate Card</a>
                 </li>
                 <li>
                     <a href="<?php echo base_url();?>index.php/Welcome/contact_us" target="_blank">Support</a>
                 </li>
             </ul>
             <button class="dwld_btn"><i class="fa fa-download" aria-hidden="true"></i>Download App</button>
         </nav>
         <!-- /#sidebar-wrapper -->

         <!-- Page Content -->
         <div id="">
             <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                 <span class="hamb-top"></span>
                 <span class="hamb-middle"></span>
                 <span class="hamb-bottom"></span>
             </button>

         </div>
         <!-- /#page-content-wrapper -->

     </div>
     <!-- /#wrapper -->



<div class="tp_header">
  <div class="col-md-2 col-sm-2 col-xs-3 tp_menu"></div>
  <div class="col-md-8 col-sm-8 col-xs-6 tp_logo"><a class="bnr_a" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>/images/logo.png" alt=""/></a> </div>

  <?php if($this->session->userdata('user') == ""){?>
    <div class="col-md-2 col-sm-2 col-xs-3 tp_login"><a class="bnr_a" style="padding-top:18px; display: block;" href="<?php echo base_url();?>index.php/Booking_controller/search_cab_login">LOGIN</a> </div>
  <?php }else{?>
    <div class="col-md-2 col-sm-2 col-xs-3 tp_login"><a class="bnr_a" href="<?php echo base_url();?>index.php/Booking_controller/user_profile"><img src="https://maxcdn.icons8.com/Share/icon/Users//user1600.png" height="30px" width="30px" alt=""/></a> </div>

<?php }?>
<div class="clear"></div>
</div>
<div class="ola-booking">
  <div class="tab-content">
     <div class="tab-pane fade in active" id="tab1">
      <form name="myForm" action="<?php echo base_url();?>index.php/Booking_controller/search_cab_city" method="post">
	
		<div class="bnr_input_group1">
			 
			 <select class='slct_input' name="city_id" id="city_id" onchange="getId(this.value);" required>
			 	<option value=""> ---Please Select City --- </option>
			 	<?php foreach($city  as $city ):?>
                         	<option value="<?= $city['city_id'] ?>"><?= $city['city_name'] ?></option>
                        	<?php endforeach; ?>
                        </select>
                        
                        <select class='slct_input' name="car_type_id" id="car_type_id"  onchange="getId1(this.value);" required>
				<option value=""> ---Please Select Car type--- </option>
			</select>
                        
                        
		 </div> 
		<div class="clear"></div>
		
	<div id="result"></div>
</div> 
<div class="clear"></div>
</div>
</div>  
</div>
</div>
 
<script>

    $(document).ready(function () {
        var trigger = $('.hamburger'),
            overlay1 = $('.overlay1'),
            isClosed = false;

        trigger.click(function () {
            hamburger_cross();
        });

        function hamburger_cross() {

            if (isClosed == true) {
                overlay1.hide();
                trigger.removeClass('is-open');
                trigger.addClass('is-closed');
                isClosed = false;
            } else {
                overlay1.show();
                trigger.removeClass('is-closed');
                trigger.addClass('is-open');
                isClosed = true;
            }
        }

        $('[data-toggle="offcanvas"]').click(function () {
            $('#wrapper1').toggleClass('toggled');
        });
    });
</script>

   <div class="web_lang hidden-xs">
                  <select onchange="javascript:window.location.href='<?php echo base_url(); ?>index.php/LanguageSwitcher/switchLang/'+this.value;">
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"';?>>English</option>
                       <option value="french" <?php if($this->session->userdata('site_lang') == 'french') echo 'selected="selected"';?>>Arabic</option>
                </select> 
</div>
        
     
</body>
</html> 
