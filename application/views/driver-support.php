<?php if($this->session->flashdata('success')):?>
     <script>alert("Query Succesfully Submited!!");</script>
<?php endif; ?>
<?php include('header_driver.php'); ?>
<script>
function validateForm() {
    var x = document.forms["myForm"]["name"].value;
	var y = document.forms["myForm"]["email"].value;
	var z = document.forms["myForm"]["phone"].value;
	var a = document.forms["myForm"]["query"].value;
   if (x == "") {
        alert("Name must be filled out");
        return false;
    }
	 else if (y == "") {
        alert("Email must be filled out");
        return false;
    }
	 
	else if (z == "") {
        alert("Please Enter Mobile Number");
        return false;
    }
	else if (a == "") {
        alert("Please Enter Query");
        return false;
    }
}
</script>
  <section class="mt-30">
    <div class="container">
      <div class="row mt-60 mb-100 pt-20 pb-20 profile_content">
        <div class="tabs-vertical-env">
          <div class="col-md-3 hidden-xs">
            <ul class="nav tabs-vertical left_tab">
              <?php if ($driver_image == "") { ?>
              <li class="driver_profile"> <img class="col-md-4 col-sm-4 col-xs-4"  src="<?php echo base_url();?>/images/profile.png"/>
                <?php } else{?>
                 <li class="driver_profile"> <img    class="col-md-4 col-sm-8 col-xs-8" src="<?php echo base_url($driver_image); ?>">
                <?php } ?>
                <div class="col-md-8 col-sm-8 col-xs-8 driver_info">
                  <div class="driver_name"><?php echo $driver_name; ?></div>
                  <div class="driver_car_name"><?php echo $car_model_name; ?></div>
                  <div class="driver_car_number"><?php echo $car_number; ?></div>
                  <div class="driver_rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i> </div>
                </div>
                <div class="clear"></div>
               	<?php if($login_logout == 1) { ?>
                <button class="btn btn-success btn-xs online_offline">Online</button>
				<?php } else { ?>
				  <button   class="btn btn-danger btn-xs online_offline">Offline</button>
				<?php } ?>
                <div class="clear"></div>
              </li>
               <li class=""> <a href="<?php echo base_url();?>index.php/Welcome/driver_profile">Profile<i class="fa fa-user" aria-hidden="true"></i></a> </li>
              <li class="">  <a href="<?php echo base_url();?>index.php/Welcome/driver_rides">My Rides<i class="fa fa-car" aria-hidden="true"></i></a> </li>
              <!--<li class=""> <a href="driver-earnings.php">Earnings <i class="fa fa-usd" aria-hidden="true"></i></a> </li>-->
              <li class=""> <a href="<?php echo base_url();?>index.php/Welcome/driver_documents">Document's<i class="fa fa-file-text" aria-hidden="true"></i></a> </li>
              <li class="">  <a href="<?php echo base_url();?>index.php/Welcome/driver_password">Change Password<i class="fa fa-lock" aria-hidden="true"></i></a> </li>
              <li class="active"> <a href="<?php echo base_url();?>index.php/Welcome/driver_suppourt">Customer Support<i class="fa fa-life-ring" aria-hidden="true"></i></a> </li>
              <li class=""> <a href="<?php echo base_url();?>index.php/Welcome/about">About us <i class="fa fa-info" aria-hidden="true"></i></a> </li>
             <!-- <li class=""> <a href="terms.php">Terms & Conditions<i class="fa fa-cogs" aria-hidden="true"></i></a> </li>-->
              <li class="">  <a href="<?php echo base_url();?>index.php/Welcome/logout">Logout<i class="fa fa-sign-out" aria-hidden="true"></i></a> </li>
            </ul>
          </div>
          <div class="tab-content col-md-9 tab_content">
            
            <div class="" id="">
             <!-- <h4 class="pb-20">Driver Support</h4> -->
              <strong>Do you have a query</strong>
              <p class="pb-40">Please let me know about your query, our support team  will get back to you </p>
              <div class="col-md-6">
                 <form name="myForm" action="<?php echo base_url();?>index.php/Useraccounts/driver_suppourt" onsubmit="return validateForm()" method="post">
                  <div class="form-group edit_profile_label">
                    <input type="text" name="name" class="form-control edit_profile_field" id="name" placeholder="Your Name" required>
                  </div>
                  <div class="form-group edit_profile_label">
                    <input type="email" name="email" class="form-control edit_profile_field" id="email" placeholder="Your Email" required>
                  </div>
                  <div class="form-group edit_profile_label">
                    <input type="text" name="phone" class="form-control edit_profile_field" id="phone" placeholder="Phone" required>
                  </div>
                  <div class="form-group edit_profile_label">
                    <textarea style="height:100px !important;" name="query" class="form-control edit_profile_field" id="query" placeholder="Your Query" required></textarea>
                  </div>
                  <div class="form-group">
                    <input type="submit" class="submit_btn" style="width:100%;" value="Submit">
                  </div>
                </form>
              </div>
            </div>
            
            
            
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php include('footer.php'); ?>
