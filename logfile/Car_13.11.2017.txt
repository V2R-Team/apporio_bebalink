Rental - Car Type Api: November 13, 2017, 12:46 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => Taxi
            [car_type_image] => uploads/car/car_27.png
            [city_id] => 56
            [base_fare] => 100 Per 4 Miles
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 28
            [car_type_name] => Ambulance
            [car_type_image] => uploads/car/car_28.png
            [city_id] => 56
            [base_fare] => 10 Per 20 Miles
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 29
            [car_type_name] => Boda Boda
            [car_type_image] => uploads/car/editcar_29.png
            [city_id] => 56
            [base_fare] => 13 Per 12 Miles
            [ride_mode] => 1
        )

)

city_name: Gurugram
latitude: 28.412344116274433
longitude: 77.04348139464854
-------------------------
Rental - Car Type Api: November 13, 2017, 12:50 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => Taxi
            [car_type_image] => uploads/car/car_27.png
            [city_id] => 56
            [base_fare] => 100 Per 4 Miles
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 28
            [car_type_name] => Ambulance
            [car_type_image] => uploads/car/car_28.png
            [city_id] => 56
            [base_fare] => 10 Per 20 Miles
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 29
            [car_type_name] => Boda Boda
            [car_type_image] => uploads/car/editcar_29.png
            [city_id] => 56
            [base_fare] => 13 Per 12 Miles
            [ride_mode] => 1
        )

)

city_name: Gurugram
latitude: 28.412344116274433
longitude: 77.04348139464854
-------------------------
Rental - Car Type Api: November 13, 2017, 3:12 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 28
            [car_type_name] => Ambulance
            [car_type_image] => uploads/car/car_28.png
            [city_id] => 121
            [base_fare] => 80 Per 2 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 29
            [car_type_name] => Boda Boda
            [car_type_image] => uploads/car/editcar_29.png
            [city_id] => 121
            [base_fare] => 90 Per 6 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 30
            [car_type_name] => Towing
            [car_type_image] => uploads/car/car_30.png
            [city_id] => 121
            [base_fare] => 80 Per 7 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [3] => Array
        (
            [car_type_id] => 31
            [car_type_name] => hire
            [car_type_image] => uploads/car/editcar_31.png
            [city_id] => 121
            [base_fare] => 200 Per 5 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [4] => Array
        (
            [car_type_id] => 32
            [car_type_name] => Hotel transfer
            [car_type_image] => uploads/car/car_32.png
            [city_id] => 121
            [base_fare] =>  Per 200 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [5] => Array
        (
            [car_type_id] => 33
            [car_type_name] => Corporate taxi
            [car_type_image] => uploads/car/editcar_33.png
            [city_id] => 121
            [base_fare] =>  Per  Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [6] => Array
        (
            [car_type_id] => 34
            [car_type_name] => school pick and drop
            [car_type_image] => uploads/car/editcar_34.png
            [city_id] => 121
            [base_fare] => 200 Per 1 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [7] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Nairobi
latitude: -1.294117493038318
longitude: 36.784051805734634
-------------------------
Rental - Car Type Api: November 13, 2017, 3:12 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 28
            [car_type_name] => Ambulance
            [car_type_image] => uploads/car/car_28.png
            [city_id] => 121
            [base_fare] => 80 Per 2 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 29
            [car_type_name] => Boda Boda
            [car_type_image] => uploads/car/editcar_29.png
            [city_id] => 121
            [base_fare] => 90 Per 6 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 30
            [car_type_name] => Towing
            [car_type_image] => uploads/car/car_30.png
            [city_id] => 121
            [base_fare] => 80 Per 7 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [3] => Array
        (
            [car_type_id] => 31
            [car_type_name] => hire
            [car_type_image] => uploads/car/editcar_31.png
            [city_id] => 121
            [base_fare] => 200 Per 5 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [4] => Array
        (
            [car_type_id] => 32
            [car_type_name] => Hotel transfer
            [car_type_image] => uploads/car/car_32.png
            [city_id] => 121
            [base_fare] =>  Per 200 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [5] => Array
        (
            [car_type_id] => 33
            [car_type_name] => Corporate taxi
            [car_type_image] => uploads/car/editcar_33.png
            [city_id] => 121
            [base_fare] =>  Per  Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [6] => Array
        (
            [car_type_id] => 34
            [car_type_name] => school pick and drop
            [car_type_image] => uploads/car/editcar_34.png
            [city_id] => 121
            [base_fare] => 200 Per 1 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [7] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Nairobi
latitude: -1.294117493038318
longitude: 36.784051805734634
-------------------------
Rental - Car Type Api: November 13, 2017, 5:07 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 28
            [car_type_name] => Ambulance
            [car_type_image] => uploads/car/car_28.png
            [city_id] => 121
            [base_fare] => 80 Per 2 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 29
            [car_type_name] => Boda Boda
            [car_type_image] => uploads/car/editcar_29.png
            [city_id] => 121
            [base_fare] => 90 Per 6 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 30
            [car_type_name] => Towing
            [car_type_image] => uploads/car/car_30.png
            [city_id] => 121
            [base_fare] => 80 Per 7 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [3] => Array
        (
            [car_type_id] => 31
            [car_type_name] => hire
            [car_type_image] => uploads/car/editcar_31.png
            [city_id] => 121
            [base_fare] => 200 Per 5 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [4] => Array
        (
            [car_type_id] => 32
            [car_type_name] => Hotel transfer
            [car_type_image] => uploads/car/car_32.png
            [city_id] => 121
            [base_fare] =>  Per 200 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [5] => Array
        (
            [car_type_id] => 33
            [car_type_name] => Corporate taxi
            [car_type_image] => uploads/car/editcar_33.png
            [city_id] => 121
            [base_fare] =>  Per  Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [6] => Array
        (
            [car_type_id] => 34
            [car_type_name] => school pick and drop
            [car_type_image] => uploads/car/editcar_34.png
            [city_id] => 121
            [base_fare] => 200 Per 1 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [7] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Nairobi
latitude: -1.2941275487565738
longitude: 36.784059517085545
-------------------------
Rental - Car Type Api: November 13, 2017, 5:36 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 28
            [car_type_name] => Ambulance
            [car_type_image] => uploads/car/car_28.png
            [city_id] => 121
            [base_fare] => 80 Per 2 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 29
            [car_type_name] => Boda Boda
            [car_type_image] => uploads/car/editcar_29.png
            [city_id] => 121
            [base_fare] => 90 Per 6 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 30
            [car_type_name] => Towing
            [car_type_image] => uploads/car/car_30.png
            [city_id] => 121
            [base_fare] => 80 Per 7 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [3] => Array
        (
            [car_type_id] => 31
            [car_type_name] => hire
            [car_type_image] => uploads/car/editcar_31.png
            [city_id] => 121
            [base_fare] => 200 Per 5 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [4] => Array
        (
            [car_type_id] => 32
            [car_type_name] => Hotel transfer
            [car_type_image] => uploads/car/car_32.png
            [city_id] => 121
            [base_fare] =>  Per 200 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [5] => Array
        (
            [car_type_id] => 33
            [car_type_name] => Corporate taxi
            [car_type_image] => uploads/car/editcar_33.png
            [city_id] => 121
            [base_fare] =>  Per  Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [6] => Array
        (
            [car_type_id] => 34
            [car_type_name] => school pick and drop
            [car_type_image] => uploads/car/editcar_34.png
            [city_id] => 121
            [base_fare] => 200 Per 1 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [7] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Nairobi
latitude: -1.2936773877303844
longitude: 36.783466413617134
-------------------------
Rental - Car Type Api: November 13, 2017, 5:56 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 28
            [car_type_name] => Ambulance
            [car_type_image] => uploads/car/car_28.png
            [city_id] => 121
            [base_fare] => 80 Per 2 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 29
            [car_type_name] => Boda Boda
            [car_type_image] => uploads/car/editcar_29.png
            [city_id] => 121
            [base_fare] => 90 Per 6 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 30
            [car_type_name] => Towing
            [car_type_image] => uploads/car/car_30.png
            [city_id] => 121
            [base_fare] => 80 Per 7 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [3] => Array
        (
            [car_type_id] => 31
            [car_type_name] => hire
            [car_type_image] => uploads/car/editcar_31.png
            [city_id] => 121
            [base_fare] => 200 Per 5 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [4] => Array
        (
            [car_type_id] => 32
            [car_type_name] => Hotel transfer
            [car_type_image] => uploads/car/car_32.png
            [city_id] => 121
            [base_fare] =>  Per 200 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [5] => Array
        (
            [car_type_id] => 33
            [car_type_name] => Corporate taxi
            [car_type_image] => uploads/car/editcar_33.png
            [city_id] => 121
            [base_fare] =>  Per  Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [6] => Array
        (
            [car_type_id] => 34
            [car_type_name] => school pick and drop
            [car_type_image] => uploads/car/editcar_34.png
            [city_id] => 121
            [base_fare] => 200 Per 1 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [7] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Nairobi
latitude: -1.2941128003697882
longitude: 36.78407359868288
-------------------------
Rental - Car Type Api: November 13, 2017, 6:10 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 28
            [car_type_name] => Ambulance
            [car_type_image] => uploads/car/car_28.png
            [city_id] => 121
            [base_fare] => 80 Per 2 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 29
            [car_type_name] => Boda Boda
            [car_type_image] => uploads/car/editcar_29.png
            [city_id] => 121
            [base_fare] => 90 Per 6 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 30
            [car_type_name] => Towing
            [car_type_image] => uploads/car/car_30.png
            [city_id] => 121
            [base_fare] => 80 Per 7 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [3] => Array
        (
            [car_type_id] => 31
            [car_type_name] => hire
            [car_type_image] => uploads/car/editcar_31.png
            [city_id] => 121
            [base_fare] => 200 Per 5 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [4] => Array
        (
            [car_type_id] => 32
            [car_type_name] => Hotel transfer
            [car_type_image] => uploads/car/car_32.png
            [city_id] => 121
            [base_fare] =>  Per 200 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [5] => Array
        (
            [car_type_id] => 33
            [car_type_name] => Corporate taxi
            [car_type_image] => uploads/car/editcar_33.png
            [city_id] => 121
            [base_fare] =>  Per  Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [6] => Array
        (
            [car_type_id] => 34
            [car_type_name] => school pick and drop
            [car_type_image] => uploads/car/editcar_34.png
            [city_id] => 121
            [base_fare] => 200 Per 1 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [7] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Nairobi
latitude: -1.2939780537411079
longitude: 36.78417317569256
-------------------------
Rental - Car Type Api: November 13, 2017, 6:12 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 28
            [car_type_name] => Ambulance
            [car_type_image] => uploads/car/car_28.png
            [city_id] => 121
            [base_fare] => 80 Per 2 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 29
            [car_type_name] => Boda Boda
            [car_type_image] => uploads/car/editcar_29.png
            [city_id] => 121
            [base_fare] => 90 Per 6 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 30
            [car_type_name] => Towing
            [car_type_image] => uploads/car/car_30.png
            [city_id] => 121
            [base_fare] => 80 Per 7 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [3] => Array
        (
            [car_type_id] => 31
            [car_type_name] => hire
            [car_type_image] => uploads/car/editcar_31.png
            [city_id] => 121
            [base_fare] => 200 Per 5 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [4] => Array
        (
            [car_type_id] => 32
            [car_type_name] => Hotel transfer
            [car_type_image] => uploads/car/car_32.png
            [city_id] => 121
            [base_fare] =>  Per 200 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [5] => Array
        (
            [car_type_id] => 33
            [car_type_name] => Corporate taxi
            [car_type_image] => uploads/car/editcar_33.png
            [city_id] => 121
            [base_fare] =>  Per  Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [6] => Array
        (
            [car_type_id] => 34
            [car_type_name] => school pick and drop
            [car_type_image] => uploads/car/editcar_34.png
            [city_id] => 121
            [base_fare] => 200 Per 1 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [7] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Nairobi
latitude: -1.29420407116534
longitude: 36.7842288315639
-------------------------
Rental - Car Type Api: November 13, 2017, 6:18 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 28
            [car_type_name] => Ambulance
            [car_type_image] => uploads/car/car_28.png
            [city_id] => 121
            [base_fare] => 80 Per 2 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 29
            [car_type_name] => Boda Boda
            [car_type_image] => uploads/car/editcar_29.png
            [city_id] => 121
            [base_fare] => 90 Per 6 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 30
            [car_type_name] => Towing
            [car_type_image] => uploads/car/car_30.png
            [city_id] => 121
            [base_fare] => 80 Per 7 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [3] => Array
        (
            [car_type_id] => 31
            [car_type_name] => hire
            [car_type_image] => uploads/car/editcar_31.png
            [city_id] => 121
            [base_fare] => 200 Per 5 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [4] => Array
        (
            [car_type_id] => 32
            [car_type_name] => Hotel transfer
            [car_type_image] => uploads/car/car_32.png
            [city_id] => 121
            [base_fare] =>  Per 200 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [5] => Array
        (
            [car_type_id] => 33
            [car_type_name] => Corporate taxi
            [car_type_image] => uploads/car/editcar_33.png
            [city_id] => 121
            [base_fare] =>  Per  Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [6] => Array
        (
            [car_type_id] => 34
            [car_type_name] => school pick and drop
            [car_type_image] => uploads/car/editcar_34.png
            [city_id] => 121
            [base_fare] => 200 Per 1 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [7] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Nairobi
latitude: -1.2941664308667715
longitude: 36.78427878767252
-------------------------
Rental - Car Type Api: November 13, 2017, 6:19 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 28
            [car_type_name] => Ambulance
            [car_type_image] => uploads/car/car_28.png
            [city_id] => 121
            [base_fare] => 80 Per 2 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 29
            [car_type_name] => Boda Boda
            [car_type_image] => uploads/car/editcar_29.png
            [city_id] => 121
            [base_fare] => 90 Per 6 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 30
            [car_type_name] => Towing
            [car_type_image] => uploads/car/car_30.png
            [city_id] => 121
            [base_fare] => 80 Per 7 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [3] => Array
        (
            [car_type_id] => 31
            [car_type_name] => hire
            [car_type_image] => uploads/car/editcar_31.png
            [city_id] => 121
            [base_fare] => 200 Per 5 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [4] => Array
        (
            [car_type_id] => 32
            [car_type_name] => Hotel transfer
            [car_type_image] => uploads/car/car_32.png
            [city_id] => 121
            [base_fare] =>  Per 200 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [5] => Array
        (
            [car_type_id] => 33
            [car_type_name] => Corporate taxi
            [car_type_image] => uploads/car/editcar_33.png
            [city_id] => 121
            [base_fare] =>  Per  Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [6] => Array
        (
            [car_type_id] => 34
            [car_type_name] => school pick and drop
            [car_type_image] => uploads/car/editcar_34.png
            [city_id] => 121
            [base_fare] => 200 Per 1 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [7] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Nairobi
latitude: -1.2940883314551606
longitude: 36.78407125174999
-------------------------
Rental - Car Type Api: November 13, 2017, 6:19 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 28
            [car_type_name] => Ambulance
            [car_type_image] => uploads/car/car_28.png
            [city_id] => 121
            [base_fare] => 80 Per 2 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 29
            [car_type_name] => Boda Boda
            [car_type_image] => uploads/car/editcar_29.png
            [city_id] => 121
            [base_fare] => 90 Per 6 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 30
            [car_type_name] => Towing
            [car_type_image] => uploads/car/car_30.png
            [city_id] => 121
            [base_fare] => 80 Per 7 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [3] => Array
        (
            [car_type_id] => 31
            [car_type_name] => hire
            [car_type_image] => uploads/car/editcar_31.png
            [city_id] => 121
            [base_fare] => 200 Per 5 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [4] => Array
        (
            [car_type_id] => 32
            [car_type_name] => Hotel transfer
            [car_type_image] => uploads/car/car_32.png
            [city_id] => 121
            [base_fare] =>  Per 200 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [5] => Array
        (
            [car_type_id] => 33
            [car_type_name] => Corporate taxi
            [car_type_image] => uploads/car/editcar_33.png
            [city_id] => 121
            [base_fare] =>  Per  Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [6] => Array
        (
            [car_type_id] => 34
            [car_type_name] => school pick and drop
            [car_type_image] => uploads/car/editcar_34.png
            [city_id] => 121
            [base_fare] => 200 Per 1 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [7] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Nairobi
latitude: -1.29413944669188
longitude: 36.7841999139979
-------------------------
Rental - Car Type Api: November 13, 2017, 6:31 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 28
            [car_type_name] => Ambulance
            [car_type_image] => uploads/car/car_28.png
            [city_id] => 121
            [base_fare] => 80 Per 2 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 29
            [car_type_name] => Boda Boda
            [car_type_image] => uploads/car/editcar_29.png
            [city_id] => 121
            [base_fare] => 90 Per 6 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 30
            [car_type_name] => Towing
            [car_type_image] => uploads/car/car_30.png
            [city_id] => 121
            [base_fare] => 80 Per 7 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [3] => Array
        (
            [car_type_id] => 31
            [car_type_name] => hire
            [car_type_image] => uploads/car/editcar_31.png
            [city_id] => 121
            [base_fare] => 200 Per 5 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [4] => Array
        (
            [car_type_id] => 32
            [car_type_name] => Hotel transfer
            [car_type_image] => uploads/car/car_32.png
            [city_id] => 121
            [base_fare] =>  Per 200 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [5] => Array
        (
            [car_type_id] => 33
            [car_type_name] => Corporate taxi
            [car_type_image] => uploads/car/editcar_33.png
            [city_id] => 121
            [base_fare] =>  Per  Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [6] => Array
        (
            [car_type_id] => 34
            [car_type_name] => school pick and drop
            [car_type_image] => uploads/car/editcar_34.png
            [city_id] => 121
            [base_fare] => 200 Per 1 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [7] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Nairobi
latitude: -1.29413928042782
longitude: 36.7841999977827
-------------------------
Rental - Car Type Api: November 13, 2017, 6:35 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 28
            [car_type_name] => Ambulance
            [car_type_image] => uploads/car/car_28.png
            [city_id] => 121
            [base_fare] => 80 Per 2 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [1] => Array
        (
            [car_type_id] => 29
            [car_type_name] => Boda Boda
            [car_type_image] => uploads/car/editcar_29.png
            [city_id] => 121
            [base_fare] => 90 Per 6 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [2] => Array
        (
            [car_type_id] => 30
            [car_type_name] => Towing
            [car_type_image] => uploads/car/car_30.png
            [city_id] => 121
            [base_fare] => 80 Per 7 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [3] => Array
        (
            [car_type_id] => 31
            [car_type_name] => hire
            [car_type_image] => uploads/car/editcar_31.png
            [city_id] => 121
            [base_fare] => 200 Per 5 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [4] => Array
        (
            [car_type_id] => 32
            [car_type_name] => Hotel transfer
            [car_type_image] => uploads/car/car_32.png
            [city_id] => 121
            [base_fare] =>  Per 200 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [5] => Array
        (
            [car_type_id] => 33
            [car_type_name] => Corporate taxi
            [car_type_image] => uploads/car/editcar_33.png
            [city_id] => 121
            [base_fare] =>  Per  Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [6] => Array
        (
            [car_type_id] => 34
            [car_type_name] => school pick and drop
            [car_type_image] => uploads/car/editcar_34.png
            [city_id] => 121
            [base_fare] => 200 Per 1 Km
            [ride_mode] => 1
            [currency_iso_code] => kes
            [currency_unicode] => 0
        )

    [7] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Nairobi
latitude: -1.293663644912996
longitude: 36.78343288600445
-------------------------
