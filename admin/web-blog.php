<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$query = "select * from web_home_pages";
$result = $db->query($query);
$list = $result->rows;
?>
<style>
    .topbnr {max-height: 400px !important;}
    .features_img{ width: 130px; margin: 0px 0px 20px 0px;}
    .page-title button {float: none;}
    .fileUpload{ float:right;}
    .tp_rht{float:right;}
</style>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Blog Page</h3>
        <span class="tp_rht">
        
        <a href="home.php?pages=web-add-blog"><button class=" btn btn-info">Add Blog <i class="ion-android-add"></i> </button></a>

        <form method="post" enctype="multipart/form-data" onSubmit="return validatelogin()">


      
   
  </span>
    </div>


  <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Datatable</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Blog id</th>
                                                    <th>Title</th>
                                                    <th>Short Desc</th>
                                                    <th width="8%">Long Desc</th>
                                                    <th>Date</th>
                                                    <th>Time</th>
                                                    <th>Image</th>
                                                    <th>Big Image</th>
                                                    <th>Edit</th>
                                                </tr>
                                            </thead>

                                     
                                            <tbody>
                                             <?php foreach($list as $page){ $i=1;?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td> <?php
                                                $title = $page['heading'];
                                                echo $title;
                                                ?></td>
                                                    
                                                    <td> <?php
                                                $title = $page['content'];
                                                echo $title;
                                                ?></td>
                                                
                                                    <td width=10%> <?php
                                                $title = $page['long_content'];
                                                echo $title;
                                                ?></td>
                                                    <td> <?php
                                                $title = $page['blog_date'];
                                                echo $title;
                                                ?></td>
                                                    <td> <?php
                                                $title = $page['blog_time'];
                                                echo $title;
                                                ?></td>
                                                    <td><img src="http://apporio.org/Alakowe/<?php echo $page['image'];?>" width="150" height="100"/></td>
                                                     <td><img src="http://apporio.org/Alakowe/webstatic/<?php echo $page['big_image'];?>" width="150" height="100"/></td>
                                                    <td>
                                                  
<a href="home.php?pages=web-add-blog&page_id=<?=$page['page_id']?>" data-original-title="Edit" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a>

                                                    </td>
                                                </tr>
                                                <?php $i++;}?>
                                              
                                                
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>

   




    

    






</div>

</form>

</section>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#topbanner').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function(){
        readURL(this);
    });







</script>
</body>
</html>
