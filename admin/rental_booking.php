<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
require 'pn_android.php';
require 'pn_iphone.php';

$query = "select * from country";
$result = $db->query($query);
$country = $result->rows;

$query = "select * from admin_panel_settings WHERE admin_panel_setting_id=1";
$result = $db->query($query);
$admin_settings = $result->row;
$admin_panel_firebase_id = $admin_settings['admin_panel_firebase_id'];
$query="select * from city";
$result = $db->query($query);
$list=$result->rows;
$query = "select * from coupons where start_date <= CURDATE() AND expiry_date >= CURDATE()";
$result = $db->query($query);
$coupons = $result->rows;
if(isset($_POST['save'])) {
    $user_id = $_POST['user_id'];
    $phonecode = $_POST['country'];
    $userphone = $_POST['userphone'];
    $phone = $phonecode.$userphone;
    $username = $_POST['username'];
    $email = $_POST['email'];
    $origin = $_POST['pac-input'];
    $car_type = $_POST['car_type'];
    $ridus = $_POST['radius'];
    $driver = $_POST['driver'];
    $ride_now = $_POST['ride_now'];
    $lat = $_POST['orig_latitude'];
    $long = $_POST['orig_longitude'];
    $datepicker = $_POST['datepicker'];
    $timepicker = $_POST['timepicker'];
    $coupon_code = $_POST['coupon_code'];
    $booking_status = 10;
    $city_id = $_POST['city_id'];
    $rental_category_id = $_POST['rental_category_id'];
    $query="select * from rentcard WHERE city_id='$city_id' AND rental_category_id='$rental_category_id'";
    $result = $db->query($query);
    $rental = $result->row;
    $rentcard_id = $rental['rentcard_id'];
    $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
    $data=$dt->format('M j');
    $day=date("l");
    $date=$day.", ".$data ;
    $time=date("H:i:s");
    $date1=date("Y-m-d");
    $last_time_stamp = date("h:i:s A");
    $ride_platform = 2;
    if (empty($user_id))
    {
        $data = "select user_id from user WHERE user_phone=$phone";
        $result = $db->query($data);
        $list = $result->row;
        if (!empty($list))
        {
            $user_id = $list['user_id'];
        }else{
            $query2="INSERT INTO user (user_phone,user_name,user_email) VALUES ('$phone','$username','$email')";
            $db->query($query2);
            $user_id = $db->getLastId();
        }
    }
    $query2="INSERT INTO rental_booking (ride_platform,user_id,rentcard_id,coupan_code,car_type_id,booking_type,pickup_lat,pickup_long,pickup_location,booking_date,booking_time,user_booking_date_time,last_update_time,booking_status) 
         VALUES ('$ride_platform','$user_id','$rentcard_id','$coupon_code','$car_type','$ride_now','$lat','$long','$origin','$datepicker','$timepicker','$date','$last_time_stamp','$booking_status')";
    $db->query($query2);
    $rental_booking_id = $db->getLastId();
    $query5="INSERT INTO table_user_rides(booking_id,ride_mode,user_id) VALUES ('$rental_booking_id','2','$user_id')";
    $db->query($query5);
    if ($driver == "")
    {
        $query = "select * from driver where online_offline = 1 and car_type_id='$car_type' and driver_admin_status=1 and busy=0 and login_logout=1";
        $result = $db->query($query);
        $list = $result->rows;
        $c = array();
        foreach($list as $login)
        {
            $driver_lat = $login['current_lat'];
            $driver_long = $login['current_long'];
            $theta = $long - $driver_long;
            $dist = sin(deg2rad($lat)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($lat)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles1 = $dist * 60 * 1.1515;
            $km=$miles1* 1.609344;
            if($km <= $ridus)
            {
                $c[] = array("driver_id"=> $login['driver_id'],"distance" => $km);
            }
        }

        if(!empty($c))
        {
            foreach ($c as $value)
            {
                $distance[] = $value['distance'];
            }
            array_multisort($distance,SORT_ASC,$c);
            $driver_id = $c[0]['driver_id'];
            $url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/Activeride/'.$driver_id.'/.json';
            $fields = array(
                'ride_id' => (string)$rental_booking_id,
                'ride_status'=>"10",
            );

            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch,CURLOPT_POST, count($fields));
            curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
            $response = curl_exec($ch);

            $query3="select * from driver_ride_allocated where driver_id='$driver_id'";
            $result3 = $db->query($query3);
            $driver_allocated = $result3->row;
            if (empty($driver_allocated))
            {
                $query5="INSERT INTO driver_ride_allocated (driver_id,ride_id,ride_mode) VALUES ('$driver_id','$rental_booking_id','2')";
                $db->query($query5);
            }else{
                $query5="UPDATE driver_ride_allocated SET ride_id='$rental_booking_id',ride_mode=2 WHERE driver_id='$driver_id'" ;
                $db->query($query5);
            }
            $query5="INSERT INTO ride_allocated (allocated_ride_id, allocated_driver_id,allocated_date) VALUES ('$rental_booking_id','$driver_id','$date1')";
            $db->query($query5);

            $query4="select * from driver where driver_id='$driver_id'";
            $result4 = $db->query($query4);
            $list4=$result4->row;
            $device_id=$list4['device_id'];
            $message = "New Rentel Booking";
            $ride_id= (String) $rental_booking_id;
            $ride_status= (String) 10;
            if($device_id!="")
            {
                if($list4['flag'] == 1)
                {
                    IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                }
                else
                {
                    AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                }
            }

            $db->redirect("home.php?pages=active-rental-rides");
        }else{
            echo '<script type="text/javascript">alert("No Driver")</script>';
            $db->redirect("home.php?pages=active-rental-rides");
        }
    }else{
        $query = "select * from driver where driver_id='$driver'";
        $result = $db->query($query);
        $list = $result->row;
        $device_id = $list['device_id'];
        $booking_status = 1;
        $query5="INSERT INTO ride_allocated (allocated_ride_id,allocated_driver_id,allocated_ride_status) VALUES ('$rental_booking_id','$driver','$booking_status')";
        $db->query($query5);
        $message = "New Rentel Booking";
        $ride_id= (String) $rental_booking_id;
        $ride_status= (String) 10;
        if($device_id  != "")
        {
            if($list['flag'] == 1)
            {
                IphonePushNotificationDriver($device_id,$message,$ride_id,$ride_status);
            }
            else
            {
                AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
            }
        }
        $url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/Activeride/'.$driver.'/.json';
        $fields = array(
            'ride_id' => (string)$ride_id,
            'ride_status'=>"1",
        );

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
        $response = curl_exec($ch);
        $db->redirect("home.php?pages=active-rental-rides");
    }

}
?>
<style>
    #map {
        height: 550px;
        width: 100%;
    }
    #infowindow-content .title {
        font-weight: bold;
    }

    #infowindow-content {
        display: none;
    }

    #map #infowindow-content {
        display: inline;
    }
    .pac-controls {
        display: inline-block;
        padding: 5px 11px;
    }

    .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }
    #pac-input:focus {
        border-color: #4d90fe;
    }
    #type-selector{
        display: none;
    }
    #strict-bounds-selector{
        display: none;
    }
</style>
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<link href="css/calander.css" rel="stylesheet" />
<script src="js/calander_jquery.js"></script>
<script src="js/calander_jquery-ui.js"></script>
<script src="js/wickedpicker.js"></script>
<link href="css/wickedpicker.css" rel="stylesheet" />
<script>
    var j = jQuery.noConflict();
    j(document).ready(function() {
        j("#datepicker").datepicker({ dateFormat: 'yy-mm-dd', minDate: 0 }).attr('readOnly', 'true');
        j('#timepicker').wickedpicker({twentyFour: true,title: 'Select Time'});
    });
</script>
<script>
     $(document).ready(function(){
        $('#get_details').on('click', function () {
            var phonecode = $('#phonecode').val();
            var phone = $('#userphone').val();
            var userphone = phonecode+phone;
            if(phone == "")
            {
                alert("Enter Phone Number");
                return false;
            }else{
                $.ajax({
                    type: "POST",
                    url: 'serach_user.php',
                    data: 'phone=' + userphone,
                    dataType: 'json',
                    success: function (data)
                    {
                        if (data != "") {
                            $('#user_id').val(data.user_id);
                            $('#username').val(data.user_name);
                            $('#email').val(data.user_email);
                        }else {
                            $("#username").val('');
                            $('#email').val('');
                            $('#user_id').val('');
                            alert("This Phone Number Is Not Registered")
                        }
                    }
                });
            }


        });
    });
   
    
    function getId(val) {
        $.ajax({
            type: "POST",
            url: "serach_renatl_package.php",
            data:  "city_id="+val,
            success:
                function(data){
                    $('#rental_category_id').html(data);
                }
        });
        $("#rental_category_id").removeAttr("disabled");
    }

    function validatelogin() {
        var userphone = document.getElementById('userphone').value;
        var username = document.getElementById('username').value;
        var city_id = document.getElementById('city_id').value;
        var rental_category_id = document.getElementById('rental_category_id').value;
        var pac = document.getElementById('pac-input').value;
        var car_type = document.getElementById('car_type').value;
        if(userphone == ""){ alert("Enter Phone Number"); return false; }
        if(username == ""){ alert("Enter Rider Name"); return false; }
        if(city_id == ""){ alert("Select City"); return false; }
        if(rental_category_id == ""){ alert("Select Rental Package"); return false; }
        if(document.getElementById('ride_now').checked == false && document.getElementById('ride_later').checked == false){
            alert("Select Ride Type");
            return false;
        }
        if(pac == ""){ alert("Enter Pickup Location"); return false; }
        if(car_type == ""){ alert("Select Car type"); return false; }
        if(document.getElementById('checkMe').checked == false && document.getElementById('driver').value == ""){
            alert("Select Driver Assign Type");
            return false;
        }
        if(document.getElementById('ride_later').checked == true && document.getElementById('datepicker').value == "")
        {
            alert("Select Ride Date");
            return false;
        }
        if(document.getElementById('ride_later').checked == true && document.getElementById('timepicker').value == "")
        {
            alert("Select Ride Time");
            return false;
        }
        if (document.getElementById('payment_option_id').value == "")
        {
            alert("Select Payment");
            return false;
        }
    }
    

    
    function disableMyText(){
        if(document.getElementById("checkMe").checked == true){
            document.getElementById("driver").disabled = true;
        }else{
            document.getElementById("driver").disabled = false;
        }
    }

    function myFunction() {
        document.getElementById("datepicker").disabled = true;
        document.getElementById("timepicker").disabled = true;
    }

    function unclickFunction() {
        document.getElementById("datepicker").disabled = false;
        document.getElementById("timepicker").disabled = false;
    }
    function setId(val){
        var city_id = document.getElementById('city_id').value;
        $.ajax({
            type: "POST",
            url: "serach_cars_rental.php",
            data: {rental_category_id: val, city_id: city_id},
            success:
                function(data){
                    $('#car_type').html(data);
                }
        });
       $("#car_type").removeAttr("disabled"); 
        
    }
    
    
    function setIdd(val){
        $.ajax({
            type: "POST",
            url: "country_code.php",
            data: "id="+val,
            success:
                function(data){
                    $('#phonecode').val(data);
                }
        });
    }

    function ssetId(val) {
        var orig_latitude = document.getElementById('orig_latitude').value;
        var orig_longitude = document.getElementById('orig_longitude').value;
        if (orig_latitude == "" && orig_longitude == ""){
            alert("Plz Enter Pickup Up and Drop Up Location")
        }else{
            $.ajax({
                type: "POST",
                url: "serach_rental_driver.php",
                data:  {car_type: val, orig_latitude: orig_latitude,orig_longitude: orig_longitude},
                success:
                    function(data){
                        $('#driver').html(data);
                    }
            });
        }
    }
</script>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title"> Rental Manual Taxi Dispatch</h3>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">

            <div class="panel-body">
                <form method="post" enctype="multipart/form-data"  onSubmit="return validatelogin()">
                    <div class="col-sm-5">

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <select class="form-control" id="country" name="country" onchange="setIdd(this.value);">
                                    <?php foreach($country as $country_list){ ?>
                                        <option value="<?php echo $country_list['id'];?>" <?php if($country_list['id'] == 110){ ?> selected <?php } ?>><?php echo $country_list['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input class="form-control" id="phonecode" value="+254" name="phonecode" type="text" readOnly>
                            </div>

                        <div class="col-md-8 col-sm-4 col-xs-12 form-group has-feedback">
                            <input class="form-control" id="userphone" name="userphone" placeholder="Enter Phone Number" type="text">
                            <input type="hidden" id="user_id" name="user_id" value="">
                            <input type="hidden" id="orig_latitude" name="orig_latitude" value="">
                            <input type="hidden" id="orig_longitude" name="orig_longitude" value="">
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                            <button type="button" id="get_details" class="btn btn-success col-md-12">Get Details</button>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <input class="form-control" id="username" name="username" placeholder="Username" type="text">
                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">
                            <input class="form-control" id="email" placeholder="Email" name="email" type="email">
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <select class="form-control" id="city_id" name="city_id" onchange="getId(this.value);">
                                <option value="">Select City</option>
                                <?php foreach($list as $city){ ?>
                                    <option value="<?php echo $city['city_id'];?>"><?php echo $city['city_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>


                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <select class="form-control" id="rental_category_id" name="rental_category_id" readonly="true"  onchange="setId(this.value);" disabled>
                                <option value="">Select Rental Package</option>
                            </select>
                        </div>


                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                            <label class="radio-inline"><input type="radio" name="ride_now" id="ride_now" onclick="myFunction()" value="1">Ride Now</label>
                            <label class="radio-inline"><input type="radio" name="ride_now" id="ride_later"  onclick="unclickFunction()"  value="2">Ride Later</label>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <input class="form-control" id="datepicker" name="datepicker" placeholder="Select Date" type="text">
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <input class="form-control" id="timepicker" name="timepicker" placeholder="Select Time" type="text">
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                            <input class="form-control" id="pac-input" name="pac-input" placeholder="Pick up Location" type="text">
                        </div>

                        <div id="type-selector" class="pac-controls">
                            <input type="radio" name="type" id="changetype-all" checked="checked">
                            <label for="changetype-all">All</label>

                            <input type="radio" name="type" id="changetype-establishment">
                            <label for="changetype-establishment">Establishments</label>

                            <input type="radio" name="type" id="changetype-address">
                            <label for="changetype-address">Addresses</label>

                            <input type="radio" name="type" id="changetype-geocode">
                            <label for="changetype-geocode">Geocodes</label>
                        </div>
                        <div id="strict-bounds-selector" class="pac-controls">
                            <input type="checkbox" id="use-strict-bounds" value="">
                            <label for="use-strict-bounds">Strict Bounds</label>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control" id="car_type" name="car_type"  onchange="ssetId(this.value);" disabled>
                                <option value="">Select vehicle type</option>
                            </select>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control" id="radius" name="radius">
                                <option value="2">2 Km Radius</option>
                                <option value="5">5 Km Radius</option>
                                <option value="10">10 Km Radius</option>
                                <option value="15">15 Km Radius</option>
                                <option value="20">20 Km Radius</option>
                            </select>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="checkbox">
                                <label>
                                    <input value="1" type="checkbox" id="checkMe" name="checkMe" onclick="disableMyText()"> Auto Assign Driver
                                </label>
                                <h4 style="text-align:center;"><b>OR</b></h4>
                            </div>
                        </div>



                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <select class="form-control" name="driver" id="driver">
                                <option value="">Select Driver</option>
                            </select>
                        </div>
                        <div class="clearfix"></div><br>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <select class="form-control" name="coupon_code" id="coupon_code">
                                <option value="">Apply Coupon</option>
                                <?php foreach ($coupons as $list_coupan):?>
                                    <option value="<?php echo $list_coupan['coupons_code']; ?>"><?php echo $list_coupan['coupons_code']."(".$list_coupan['coupons_price'].")"; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="clearfix"></div><br>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <select class="form-control" name="payment_option_id" id="payment_option_id">
                                <option value="">Select Payment Method</option>
                                <option value="1">Cash</option>
                            </select>
                        </div>
                        <div class="clearfix"></div><br>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <input type="submit" class="btn btn-success" id="save" name="save" value="Book Now" >
                        </div>
                        <div class="clear"></div>
                    </div>

                </form>
                <div class="col-sm-7">
                    <div class="col-sm-12">
                        <div class="row">

                            <div id="map">
                            </div>
                            <div id="infowindow-content">
                                <img src="" width="16" height="16" id="place-icon">
                                <span id="place-name"  class="title"></span><br>
                                <span id="place-address"></span>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
</div>


<script>
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: <?php echo $admin_settings['admin_panel_latitude']?>, lng: <?php echo $admin_settings['admin_panel_longitude']?>},
            zoom: 13
        });
        //  var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var options = {
            componentRestrictions: {country: "KE"}
        };
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        var autocomplete = new google.maps.places.Autocomplete(input,options);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                var lat = place.geometry.location.lat();
                var long = place.geometry.location.lng();
                document.getElementById("orig_latitude").value = lat;
                document.getElementById("orig_longitude").value = long;
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            infowindowContent.children['place-icon'].src = place.icon;
            infowindowContent.children['place-name'].textContent = place.name;
            infowindowContent.children['place-address'].textContent = address;
            infowindow.open(map, marker);
        });
        function setupClickListener(id, types) {
            var radioButton = document.getElementById(id);
            radioButton.addEventListener('click', function() {
                autocomplete.setTypes(types);
            });
        }

        setupClickListener('changetype-all', []);
        setupClickListener('changetype-address', ['address']);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);

        document.getElementById('use-strict-bounds')
            .addEventListener('click', function() {
                console.log('Checkbox clicked! New state=' + this.checked);
                autocomplete.setOptions({strictBounds: this.checked});
            });
    }
    
    
       function alertbox() { 
    $("#car_type").click(function () {
    if (this.readOnly) {
        alert("The textbox is clicked.");
    }
});
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $admin_settings['admin_panel_map_key']?>&libraries=places&callback=initMap"
        async defer></script>
</body>
</html>