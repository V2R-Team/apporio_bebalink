<?php

include_once '../apporioconfig/start_up.php';

$company_id = $_SESSION['Company']['ID'];
$query ="select * from done_ride INNER JOIN payment_confirm ON done_ride.done_ride_id=payment_confirm.order_id INNER JOIN ride_table ON done_ride.ride_id=ride_table.ride_id INNER JOIN user ON ride_table.user_id=user.user_id INNER JOIN driver ON ride_table.driver_id=driver.driver_id";
$result = $db->query($query);
$data = $result->rows;

    function filterData(&$str)
    {
        $str = preg_replace("/\t/", "\\t", $str);
        $str = preg_replace("/\r?\n/", "\\n", $str);
        if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
    }
    
    // file name for download
    $fileName = "codexworld_export_data" . date('Ymd') . ".xls";
    
    // headers for download
    header("Content-Disposition: attachment; filename=\"$fileName\"");
    header("Content-Type: application/vnd.ms-excel");
    
    $flag = false;
    foreach($data as $row) {
        if(!$flag) {
            // display column names as first row
            echo implode("\t", array_keys($row)) . "\n";
            $flag = true;
        }
        // filter data
        array_walk($row, 'filterData');
        echo implode("\t", array_values($row)) . "\n";

    }
    
    exit;
?>