<?php
include_once '../apporioconfig/start_up.php';
date_default_timezone_set('Africa/Lagos');      
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
$page_id = $_GET['page_id'];
$query = "select * from web_home_pages where page_id='$page_id'";
$result = $db->query($query);
$text = $result->row;



if(isset($_POST['savechanges']))
{
    $title = addslashes($_POST['title']);
    $shortdesciption = addslashes($_POST['shortdescription']);
    $longdescription= addslashes($_POST['longdescription']);
    
       $iddd= $_POST['savechanges'];
        $time = date("H:i:s");
        $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
        $data=$dt->format('M j');
        $day=date("l");
        $date=$day.", ".$data;
  
   if($page_id) {
        $query2 = "UPDATE web_home_pages SET heading='$title',content='$shortdesciption ',long_content='$longdescription',blog_time='$time',blog_date='$date' WHERE page_id='$iddd'";
        $db->query($query2);
        
        if(!empty($_FILES['image'])) 
  {
  
   $img_name = $_FILES['image']['name'];

   $filedir  = "../uploads/websitebloppages/";
   if(!is_dir($filedir)) mkdir($filedir, 0755, true);
   $fileext = strtolower(substr($_FILES['image']['name'],-4));
   if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg") 
   {
    if($fileext=="jpeg") 
    {
     $fileext=".jpg";
    }
    $pfilename ="blog_1".$time.$fileext;
    $filepath1 = "uploads/websitebloppages/".$pfilename;
    $filepath = $filedir.$pfilename;
    copy($_FILES['image']['tmp_name'], $filepath);
    $upd_qry = "UPDATE web_home_pages SET image ='$filepath1' where page_id ='$iddd'";
    $db->query($upd_qry);
   }
  }
  
  
  if(!empty($_FILES['bigimage'])) 
  {
 
   $img_name = $_FILES['bigimage']['name'];

   $filedir  = "../uploads/websitebloppages/";
   if(!is_dir($filedir)) mkdir($filedir, 0755, true);
   $fileext = strtolower(substr($_FILES['bigimage']['name'],-4));
   if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg") 
   {
    if($fileext=="jpeg") 
    {
     $fileext=".jpg";
    }
    $pfilename = "blog_2".$time.$fileext;
    $filepath1 = "uploads/websitebloppages/".$pfilename;
    $filepath = $filedir.$pfilename;
    copy($_FILES['bigimage']['tmp_name'], $filepath);

    $upd_qry = "UPDATE web_home_pages SET big_image='$filepath1' where page_id ='$iddd'";
    $db->query($upd_qry);
   }
  }
  
  
       
        }
        else
        {
        $sql = "INSERT INTO web_home_pages (heading,content,long_content,blog_date,blog_time) VALUES ('$title','$shortdesciption','$longdescription','$date','$time')";
        
        $db->query($sql);
        $blog_id = $db->getLastId();
 
 
 if(!empty($_FILES['image'])) 
  {
  
   $img_name = $_FILES['image']['name'];

   $filedir  = "../uploads/websitebloppages/";
   if(!is_dir($filedir)) mkdir($filedir, 0755, true);
   $fileext = strtolower(substr($_FILES['image']['name'],-4));
   if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg") 
   {
    if($fileext=="jpeg") 
    {
     $fileext=".jpg";
    }
    $pfilename ="blog_1".$time.$fileext;
    $filepath1 = "uploads/websitebloppages/".$pfilename;
    $filepath = $filedir.$pfilename;
    copy($_FILES['image']['tmp_name'], $filepath);
    $upd_qry = "UPDATE web_home_pages SET image ='$filepath1' where page_id ='$blog_id'";
    $db->query($upd_qry);
   }
  }
  
  
  if(!empty($_FILES['bigimage'])) 
  {
   $img_name = $_FILES['bigimage']['name'];
   $filedir  = "../uploads/websitebloppages/";
   if(!is_dir($filedir)) mkdir($filedir, 0755, true);
   $fileext = strtolower(substr($_FILES['bigimage']['name'],-4));
   if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg") 
   {
    if($fileext=="jpeg") 
    {
     $fileext=".jpg";
    }
    $pfilename = "blog_2".$time.$fileext;
    $filepath1 = "uploads/websitebloppages/".$pfilename;
    $filepath = $filedir.$pfilename;
    copy($_FILES['bigimage']['tmp_name'], $filepath);

    $upd_qry = "UPDATE web_home_pages SET big_image='$filepath1' where page_id ='$blog_id'";
    $db->query($upd_qry);
   }
  }
  }
 $db->redirect("home.php?pages=web-blog"); 
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Taxi Admin Panel">
    <link rel="icon" href="img/favicon.jpg" sizes="50x50" />

    <title>Taxi Admin</title>
    <?php include('style.php'); ?>

    <!--bootstrap-wysihtml5-->
    <link rel="stylesheet" type="text/css" href="taxi/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link href="taxi/summernote/summernote.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">

</head>

<body>
<?php include('sidebar.php'); ?>
<section class="content">
    <?php include('header.php'); ?>
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">Add Blog</h3>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="form" >
                            <form class="cmxform form-horizontal tasi-form" enctype="multipart/form-data"  method="post">

                            


                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Title</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" placeholder="Title" name="title" value="<?=$text['heading'];?>" id="">
                                    </div>
                                </div>



                                  <div class="form-group">
                                    <label class="col-md-2 control-label">Short Description</label>
                                    <div class="col-md-10">

                                        <textarea class="summernote" name="shortdescription" placeholder="shortdescription" rows="5"><?= $text['content'];?></textarea>
                                    </div>
                                </div>
                                
                                  <div class="form-group">
                                    <label class="col-md-2 control-label">Long Description</label>
                                    <div class="col-md-10">

                                        <textarea class="summernote" name="longdescription" placeholder="longdescription" rows="5"><?= $text['long_content'];?></textarea>
                                    </div>
                                </div>
                                
                                     <div class="form-group">
                                    <label class="col-md-2 control-label">Blog Image</label>
                                    <div class="col-md-10">

                                       <input type="file" class="form-control" name="image" id="image" value="" placeholder="Blog Image">
                                    </div>
                                </div>
                                
                                     <div class="form-group">
                                    <label class="col-md-2 control-label">Big Blog Image</label>
                                    <div class="col-md-10">

                                        <input type="file" class="form-control" name="bigimage" id="bigimage" value="" placeholder="Blog Image">
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                      <button type="submit" name="savechanges" value="<?php echo $text['page_id'];?>" class="btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white">Save </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- .form -->

                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel -->
            </div>
            <!-- col -->

        </div>
        <!-- End row -->

    </div>





    </div>
    <!-- Page Content Ends -->
    <!-- ================== -->

    <!-- Footer Start -->
    <?php include('footer.php'); ?>
    <!-- Footer Ends -->



</section>





<!-- js placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/pace.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.nicescroll.js" type="text/javascript"></script>

<script type="text/javascript" src="taxi/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="taxi/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<!--form validation init-->
<script src="taxi/summernote/summernote.min.js"></script>

<script src="js/jquery.app.js"></script>

<script>

    jQuery(document).ready(function(){
        $('.wysihtml5').wysihtml5();

        $('.summernote').summernote({
            height: 200,                 // set editor height

            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor

            focus: true                 // set focus to editable area after initializing summernote
        });

    });
</script>


</body>
</html>
