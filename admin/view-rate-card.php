<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
$where = "";
if(isset($_POST['city'])) {
    $city = $_POST["city"];
    $where .= " and city.city_id LIKE '%$city%'";
}else{
    $city=3;
    $where .= " and city.city_id LIKE '%$city%'";
}
$query="select * from price_card INNER JOIN city ON price_card.city_id=city.city_id INNER JOIN car_type ON price_card.car_type_id=car_type.car_type_id where 1=1 $where";
$result = $db->query($query);
$list=$result->rows;
$query="select * from extra_charges INNER JOIN city ON extra_charges.city_id=city.city_id where 1=1 $where";
$result = $db->query($query);
$list1=$result->rows;
$query="select city_name,city_id from city";
$result=$db->query($query);
$list12=$result->rows;
if(isset($_POST['savechanges']))
{
    $query2="UPDATE price_card SET distance_unit='".$_POST['distance']."',commission='".$_POST['commission']."',base_distance='".$_POST['base_distance']."',base_distance_price='".$_POST['base_distance_price']."',base_price_per_unit='".$_POST['base_price_per_unit']."',free_waiting_time='".$_POST['free_waiting_time']."',wating_price_minute='".$_POST['wating_price_minute']."',free_ride_minutes='".$_POST['free_ride_minutes']."',price_per_ride_minute='".$_POST['price_per_ride_minute']."' where price_id='".$_POST['savechanges']."'";
    $db->query($query2);
    $db->redirect("home.php?pages=view-rate-card");
}

if (isset($_POST['delete'])) {
    $delqry1 = "DELETE from price_card where price_id='" . $_POST['delete'] . "'";
    $db->query($delqry1);
    $db->redirect("home.php?pages=view-rate-card");
}

?>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Fare Card</h3>
        <span class="tp_rht">
      <a href="home.php?pages=add-peak-time-charges"  data-toggle="tooltip" title="Extra Charge" class="btn btn-pink"><i class="fa fa-bell" aria-hidden="true"></i> </i>Add Peak Time Charges</a>
<span>
     <span class="tp_rht">
      <a href="home.php?pages=add-night-time-charges" data-toggle="tooltip" style="margin-left: 8px; background-color: black;border: black;" title="Extra Charge" class="btn btn-pink"><i class="fa fa-bell" aria-hidden="true"></i> </i>Add Night Time Charges</a>
<span>
       <span class="tp_rht">
            <a href="home.php?pages=add-rate-card" data-toggle="tooltip" title="" class="btn btn-primary add_btn" data-original-title="Add Rate Card"><i class="fa fa-plus"></i></a>
           </span>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
            <form method="post" >
                <div class="" style="margin: 10px 0px 30px 0px;">
                    <div class="form-group col-md-3">
                        <select class="form-control" name="city" id="city">
                            <?php foreach ($list12 as $data){  ?>
                                <option value="<?= $data['city_id']; ?>" <?php if($data['city_id'] == $city){ ?> selected <?php } ?>><?= $data['city_name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <button class="btn btn-primary" type="submit" name="seabt12"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>

        </div>
        <table class="table table-striped table-bordered table-responsive">
            <thead>
            <tr>
                <th>Car Type</th>
                <th>Minimum Bill</th>
                <th>After Minimum Bill</th>
                <th>Waiting Charge</th>
                <th>Time Charge</th>
                <th>Edit</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($list as $ratecard){ ?>
                <tr>
                    <td><?= $ratecard['car_type_name']; ?></td>
                    <td><?php
                        $currency = $ratecard['currency'];
                        $base_distance = $ratecard['base_distance'];
                        $base_distance_price = $ratecard['base_distance_price'];
                        $distance_unit = $ratecard['distance_unit'];
                        echo $currency." ".$base_distance_price." for first ".$base_distance." ".$distance_unit;
                        ?></td>
                    <td><?php
                        $currency = $ratecard['currency'];
                        $base_price_per_unit = $ratecard['base_price_per_unit'];
                        echo  $currency." ".$base_price_per_unit." per ".$distance_unit;
                        ?></td>
                    <td><?php
                        $currency = $ratecard['currency'];
                        $free_waiting_time = $ratecard['free_waiting_time'];
                        $wating_price_minute = $ratecard['wating_price_minute'];
                        echo $currency." ".$wating_price_minute." Per Min after First ".$free_waiting_time." Free Min";
                        ?></td>
                    <td><?php
                        $currency = $ratecard['currency'];
                        $free_ride_minutes = $ratecard['free_ride_minutes'];
                        $price_per_ride_minute = $ratecard['price_per_ride_minute'];
                        echo $currency." ".$price_per_ride_minute." Per Min after First ".$free_ride_minutes." Free Min";
                        ?></td>
                    <td>
                        <span data-target="#edit<?php echo $ratecard['price_id'];?>" data-toggle="modal"><a data-original-title="Edit"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>
                        <span data-target="#delete<?php echo $ratecard['price_id'];?>" data-toggle="modal"><a data-original-title="delete"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-trash"></i> </a></span>

                    </td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>
    </div>
</div>
</div>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Extra Charges</h3>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
            <form method="post" >
                <div class="" style="margin: 10px 0px 30px 0px;">
                    <div class="form-group col-md-3">
                        <select class="form-control" name="city" id="city">
                            <?php foreach ($list12 as $data){  ?>
                                <option value="<?= $data['city_id']; ?>" <?php if($data['city_id'] == $city){ ?> selected <?php } ?>><?= $data['city_name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <button class="btn btn-primary" type="submit" name="seabt12"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>

        </div>
        <table class="table table-striped table-bordered table-responsive">
            <thead>
            <tr>
                <th>Day</th>
                <th>Slot One Starttime</th>
                <th>Slot One Endtime</th>
                <th>Slot Two Starttime</th>
                <th>Slot Two Endtime</th>
                <th>Charges Type</th>
                <th>Charges</th>
                <th>Edit</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($list1 as $extra){ ?>
                <tr>
                    <td><?= $extra['extra_charges_day']; ?></td>
                    <td><?=  $extra['slot_one_starttime'];?></td>
                    <td><?= $extra['slot_one_endtime']; ?></td>
                    <td><?= $extra['slot_two_starttime'];?></td>
                    <td><?= $extra['slot_two_endtime'];?></td>
                    <td><?php $payment_type =  $extra['payment_type'];
                        if($payment_type == 1){
                            echo "Nominal";
                        }else{
                            echo "Multiplier";
                        }
                        ?></td>
                    <td><?php
                        $price =  $extra['slot_price'];
                        $currency = $extra['currency'];
                        echo $currency." ".$price;
                        ?></td>
                    <td><a data-original-title="Edit" href="home.php?pages=edit-extra-charges&id=<?=$extra['extra_charges_id']?>" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a>
                    </td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>
    </div>
</div>
</div>

<!--DELETE TYPE-->

<?php foreach($list as $ratecard){ ?>
    <div class="modal fade" id="delete<?php echo $ratecard['price_id'];?>" role="dialog">
        <div class="modal-dialog">
            <form method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title fdetailsheading">Delete Vehicle Type</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <h3>Do You Really Want To Delete This Vehicle Type?</h3></div>
                        <div class="modal-footer">
                            <button type="submit" name="delete" value="<?php echo $ratecard['price_id'];?>"
                                    class="btn btn-danger">Delete
                            </button>
                            <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
<?php } ?>


<?php foreach($list as $ratecard){ ?>
    <div class="modal fade" id="edit<?php echo $ratecard['price_id'];?>" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content starts-->
            <form method="post" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Edit Rate Card</h4>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Base Ride Distance</label>
                                <input type="text" class="form-control"  placeholder="Base Ride Distance" name="base_distance" value="<?php echo $ratecard['base_distance'];?>" id="base_distance" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Base Ride Distance Charges</label>
                                <input type="text" class="form-control"  placeholder="Base Ride Distance Charges" name="base_distance_price" value="<?php echo $ratecard['base_distance_price'];?>" id="base_distance_price" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Price Per Unit</label>
                                <input type="text" class="form-control"  placeholder="Price Per Unit" name="base_price_per_unit" value="<?php echo $ratecard['base_price_per_unit'];?>" id="base_price_per_unit" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Free waiting Time</label>
                                <input type="text" class="form-control"  placeholder="Free waiting Time" name="free_waiting_time" value="<?php echo $ratecard['free_waiting_time'];?>" id="free_waiting_time" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Waiting Price Per Minute</label>
                                <input type="text" class="form-control"  placeholder="Waiting Price Per Minute" name="wating_price_minute" value="<?php echo $ratecard['wating_price_minute'];?>" id="wating_price_minute" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Free Ride Minute</label>
                                <input type="text" class="form-control"  placeholder="Free Ride Minute" name="free_ride_minutes" value="<?php echo $ratecard['free_ride_minutes'];?>" id="free_ride_minutes" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Price Per Ride Minute</label>
                                <input type="text" class="form-control"  placeholder="Price Per Ride Minute" name="price_per_ride_minute" value="<?php echo $ratecard['price_per_ride_minute'];?>" id="price_per_ride_minute" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Commission</label>
                                <input type="text" class="form-control"  placeholder="Commission" name="commission" value="<?php echo $ratecard['commission'];?>" id="free_ride_minutes" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Distance Unit</label>
                                <select class="form-control" name="distance" id="distance" required>
                                    <option value="">Select Distance Unit</option>
                                    <option value="Miles" <?php if($ratecard['distance_unit'] == "Miles"){ ?> selected <?php } ?>>Miles</option>
                                    <option value="Km" <?php if($ratecard['distance_unit'] == "Km"){ ?> selected <?php } ?>>Kilometers</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" name="savechanges" value="<?php echo $ratecard['price_id'];?>" class="btn btn-info">Save Changes</button>
                </div>
            </div>
            </form>
        </div>
    </div>
<?php }?>
</section>

</body></html>