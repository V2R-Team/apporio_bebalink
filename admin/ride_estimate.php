<?php
include_once '../apporioconfig/start_up.php';
$car_type = $_REQUEST['car_type'];
$orig_latitude = $_REQUEST['orig_latitude'];
$orig_longitude = $_REQUEST['orig_longitude'];
$distance = $_REQUEST['distance'];
$driver = $_REQUEST['driver'];
if ($driver != "")
{
    $data="select * from driver WHERE driver_id='$driver'";
    $result = $db->query($data);
    $list=$result->row;
    $city_id = $list['city_id'];
    $car_type_id = $list['car_type_id'];
    $query="select * from price_card where city_id='$city_id' and car_type_id='$car_type_id'";
    $result = $db->query($query);
    $list3 = $result->row;
    $distance_unit = $list3['distance_unit'];
    if($distance_unit == "Km"){
        $distance1 = ($distance/1000);
    }else{
        $distance1 = $distance * 0.00062137;
    }
    $base_distance = $list3['base_distance'];
    $base_distance_price = $list3['base_distance_price'];
    $base_price_per_unit = $list3['base_price_per_unit'];

    if($distance1 <= $base_distance)
    {
        $final_amount = $base_distance_price;
        $final_amount_string =(String)$final_amount;
        $final_amount_string = number_format($final_amount_string , 2, '.', '');
    }
    else
    {
        $diff_distance = $distance1-$base_distance;
        $amount1=($diff_distance * $base_price_per_unit);
        $final_amount = $base_distance_price+$amount1;
        $final_amount_string =(String)$final_amount;
        $final_amount_string =(String)$final_amount;
        $final_amount_string = number_format($final_amount_string , 2, '.', '');
    }
    echo $final_amount_string." ₦";
}else{
    $q="select * from driver WHERE verification_status=1 AND car_type_id='$car_type' AND driver_category IN (1,3) AND online_offline=1 and driver_admin_status=1 and busy=0 and login_logout=1";
    $result = $db->query($q);
    $list=$result->rows;
    $c = array();
    foreach($list as $login3)
    {
        $driver_lat = $login3['current_lat'];
        $driver_long =$login3['current_long'];

        $theta = $orig_longitude - $driver_long;
        $dist = sin(deg2rad($orig_latitude)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($orig_latitude)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $km=$miles* 1.609344;
        $km = sprintf("%.2f",$km);
        if($km <= 10)
        {
            $c[] = array("driver_id"=> $login3['driver_id'],"city_id" => $login3['city_id'],"driver_name"=>$login3['driver_name'],"distance" => $km,);
        }
    }
    if (!empty($c))
    {
        foreach ($c as $value)
        {
            $distance[] = $value['distance'];
        }
        array_multisort($distance,SORT_ASC,$c);
        $city_id = $c[0]['city_id'];
        $query="select * from price_card where city_id='$city_id' and car_type_id='$car_type'";
        $result = $db->query($query);
        $list3 = $result->row;
        $distance_unit = $list3['distance_unit'];
        if($distance_unit == "Km"){
            $distance1 = ($distance/1000);
        }else{
            $distance1 = $distance * 0.00062137;
        }
        $base_distance = $list3['base_distance'];
        $base_distance_price = $list3['base_distance_price'];
        $base_price_per_unit = $list3['base_price_per_unit'];
        if($distance1 <= $base_distance)
        {
            $final_amount = $base_distance_price;
            $final_amount_string =(String)$final_amount;
            $final_amount_string = number_format($final_amount_string , 2, '.', '');
        }
        else
        {
            $diff_distance = $distance1-$base_distance;
            $amount1=($diff_distance * $base_price_per_unit);
            $final_amount = $base_distance_price+$amount1;
            $final_amount_string =(String)$final_amount;
            $final_amount_string =(String)$final_amount;
            $final_amount_string = number_format($final_amount_string , 2, '.', '');
        }
        echo $final_amount_string." ₦";
    }
}