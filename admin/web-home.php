<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

$query="select * from web_home where id=1";
$result = $db->query($query);
$list=$result->row;

$query21="select * from web_home where id=2";
$result21 = $db->query($query21);
$list1=$result21->row;
 

if(isset($_POST['save']))
{
    $query1="UPDATE web_home SET features2_desc='".$_POST['features2_desc']."',features2_heading='".$_POST['features2_heading']."',features1_desc='".$_POST['features1_desc']."',features1_heading='".$_POST['features1_heading']."',parallax_details='".$_POST['parallax_details']."',parallax_heading2='".$_POST['parallax_heading2']."',parallax_heading1='".$_POST['parallax_heading1']."',heading3_details='".$_POST['heading3_details']."',heading3='".$_POST['heading3']."',heading2_details='".$_POST['heading2_details']."',heading2='".$_POST['heading2']."',heading1_details='".$_POST['heading1_details']."',heading1='".$_POST['heading1']."',itunes_url='".$_POST['itunes_url']."',google_play_url='".$_POST['google_play_url']."',market_places_desc='".$_POST['market_places_desc']."',app_details='".$_POST['app_details']."',app_heading1='".$_POST['app_heading1']."',app_name='".$_POST['app_name']."',web_title='".$_POST['web_title']."',web_footer='".$_POST['web_footer']."',app_heading='".$_POST['app_heading']."' WHERE id=1";
    $db->query($query1);
	
	  $query1="UPDATE web_home SET features2_desc='".$_POST['features2_desc']."',features2_heading='".$_POST['features2_heading']."',features1_desc='".$_POST['features1_desc']."',features1_heading='".$_POST['features1_heading']."',parallax_details='".$_POST['parallax_details']."',parallax_heading2='".$_POST['parallax_heading2']."',parallax_heading1='".$_POST['parallax_heading1']."',heading3_details='".$_POST['heading3_details']."',heading3='".$_POST['heading3']."',heading2_details='".$_POST['heading2_details']."',heading2='".$_POST['heading2']."',heading1_details='".$_POST['heading1_details']."',heading1='".$_POST['heading1']."',itunes_url='".$_POST['itunes_url']."',google_play_url='".$_POST['google_play_url']."',market_places_desc='".$_POST['market_places_desc']."',app_details='".$_POST['app_details']."',app_heading1='".$_POST['app_heading1']."',app_name='".$_POST['app_name1']."',web_title='".$_POST['web_title1']."',web_footer='".$_POST['web_footer1']."',app_heading='".$_POST['app_heading1']."' WHERE id=2";
    $db->query($query1);

    if(!empty($_FILES['features2_bg']))
    {

        $imgInp= $_FILES['features2_bg']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['features2_bg']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "features2_bg".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['features2_bg']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET features2_bg='$filepath1'  WHERE id=1";
            $db->query($query1);
			$query11="UPDATE web_home SET features2_bg='$filepath1'  WHERE id=2";
            $db->query($query11);
        }
    }


    if(!empty($_FILES['features1_bg']))
    {

        $imgInp= $_FILES['features1_bg']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['features1_bg']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "features1_bg".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['features1_bg']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET features1_bg='$filepath1'  WHERE id=1";
            $db->query($query1);
			  $query11="UPDATE web_home SET features1_bg='$filepath1'  WHERE id=2";
            $db->query($query11);
        }
    }

    if(!empty($_FILES['parallax_bg']))
    {

        $imgInp= $_FILES['parallax_bg']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['parallax_bg']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "parallax_bg".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['parallax_bg']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET parallax_bg='$filepath1'  WHERE id=1";
            $db->query($query1);
			  $query11="UPDATE web_home SET parallax_bg='$filepath1'  WHERE id=2";
            $db->query($query11);
        }
    }


    if(!empty($_FILES['parallax_screen']))
    {

        $imgInp= $_FILES['parallax_screen']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['parallax_screen']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "parallax_screen".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['parallax_screen']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET parallax_screen='$filepath1'  WHERE id=1";
            $db->query($query1);
			 $query11="UPDATE web_home SET parallax_screen='$filepath1'  WHERE id=2";
            $db->query($query11);
        }
    }

    if(!empty($_FILES['heading3_img']))
    {

        $imgInp= $_FILES['heading3_img']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['heading3_img']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "heading3_img".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['heading3_img']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET heading3_img='$filepath1'  WHERE id=1";
            $db->query($query1);
			$query11="UPDATE web_home SET heading3_img='$filepath1'  WHERE id=2";
            $db->query($query11);
        }
    }


    if(!empty($_FILES['heading2_img']))
    {

        $imgInp= $_FILES['heading2_img']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['heading2_img']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "heading2_img".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['heading2_img']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET heading2_img='$filepath1'  WHERE id=1";
            $db->query($query1);
			 $query11="UPDATE web_home SET heading2_img='$filepath1'  WHERE id=2";
            $db->query($query11);
        }
    }

    if(!empty($_FILES['heading1_img']))
    {

        $imgInp= $_FILES['heading1_img']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['heading1_img']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "heading1_img".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['heading1_img']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET heading1_img='$filepath1'  WHERE id=1";
            $db->query($query1);
			$query11="UPDATE web_home SET heading1_img='$filepath1'  WHERE id=2";
            $db->query($query11);
        }
    }


    if(!empty($_FILES['itunes_btn']))
    {

        $imgInp= $_FILES['itunes_btn']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['itunes_btn']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "itunes_btn".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['itunes_btn']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET itunes_btn='$filepath1'  WHERE id=1";
            $db->query($query1);
			$query11="UPDATE web_home SET itunes_btn='$filepath1'  WHERE id=2";
            $db->query($query11);
        }
    }

    if(!empty($_FILES['banner_img']))
    {

        $imgInp= $_FILES['banner_img']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['banner_img']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "banner_".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['banner_img']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET banner_img='$filepath1'  WHERE id=1";
            $db->query($query1);
			$query11="UPDATE web_home SET banner_img='$filepath1'  WHERE id=2";
            $db->query($query11);
        }
    }

    if(!empty($_FILES['app_screen1'])){

        $imgInp= $_FILES['app_screen1']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['app_screen1']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "app_screen1".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['app_screen1']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET app_screen1='$filepath1'  WHERE id=1";
            $db->query($query1);
			$query1="UPDATE web_home SET app_screen1='$filepath1'  WHERE id=1";
            $db->query($query1);
        }
    }

    if(!empty($_FILES['app_screen2'])){

        $imgInp= $_FILES['app_screen2']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['app_screen2']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "app_screen2".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['app_screen2']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET app_screen2='$filepath1'  WHERE id=1";
            $db->query($query1);
			$query11="UPDATE web_home SET app_screen2='$filepath1'  WHERE id=2";
            $db->query($query11);
        }
    }

    if(!empty($_FILES['google_play_btn'])){

        $imgInp= $_FILES['google_play_btn']['name'];
        $filedir  = "../uploads/website/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['google_play_btn']['name'],-4));

        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "google_play_btn".time().$fileext;
            $filepath1 = "uploads/website/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['google_play_btn']['tmp_name'], $filepath);
            $query1="UPDATE web_home SET google_play_btn='$filepath1'  WHERE id=1";
            $db->query($query1);
			$query11="UPDATE web_home SET google_play_btn='$filepath1'  WHERE id=2";
            $db->query($query11);
        }
    }



    $db->redirect("home.php?pages=web-home");
}



?>
<style>
    .topbnr {max-height: 400px !important;}
    .features_img{ width: 130px; margin: 0px 0px 20px 0px;}
    .page-title button {float: none;}
    .fileUpload{ float:right;}
    .tp_rht{float:right;}
</style>
<script>
    function validatelogin() {
            //alert("This is Demo Version");
            //return false;
    }
</script>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Home Page</h3>
        <span class="tp_rht">

        <form method="post" enctype="multipart/form-data" onSubmit="return validatelogin()">


      <input type="submit" class=" btn btn-info" id="save" name="save" value="Save" >
   
  </span>
    </div>


    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">SEO</h3></div>
                <div class="panel-body">
                
                    <div class="form-group">
                        <label for="">Title</label>
                        <input type="text" class="form-control" name="web_title" id="web_title" value="<?php echo $list['web_title'];?>" placeholder="Website Title">
                    </div>
					 <div class="form-group">
                        <label for="">Title In Arabic</label>
                        <input type="text" class="form-control" name="web_title1" id="web_title1" value="<?php echo $list1['web_title'];?>" placeholder="Website Title">
                    </div>
					
                    
                    <div class="form-group">
                        <label for="">Application Name</label>                        
                         <input type="text" class="form-control" id="app_name" name="app_name"  value="<?php echo $list['app_name'];?>"  placeholder="Application Name.."></textarea>
                    </div>
				 <div class="form-group">
                        <label for="">Application Name In Arabic</label>                        
                         <input type="text" class="form-control" id="app_name1" name="app_name1"  value="<?php echo $list1['app_name'];?>"  placeholder="Application Name.."></textarea>
                    </div>
                    
                </div>
            </div>
        </div>

   <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Footer</h3></div>
                <div class="panel-body">
                    <div class="form-group">
                    	<label for="">Footer</label> 
                        <input type="text" class="form-control" name="web_footer" id="web_footer" value="<?php echo $list['web_footer'];?>" placeholder="Copyright">
                    </div>
					<div class="form-group">
                    	<label for="">Footer In Arabic</label> 
                        <input type="text" class="form-control" name="web_footer1" id="web_footer1" value="<?php echo $list1['web_footer'];?>" placeholder="Copyright">
                    </div>
					
                     <div class="form-group">
                        <label for="">Meta Description</label>                        
                         <textarea class="form-control" id="meta_desc" name="meta_desc" placeholder="Meta Description"></textarea>
                        
                    </div>
                </div>
            </div>
        </div>
      </div>  
       

    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading row">
                    <h3 class="panel-title col-md-6">Banner</h3>
                    <span class=" col-md-6">
          <div class="fileUpload btn btn-purple"> <span><i class="ion-upload m-r-5"></i>Upload</span>

               <input type="file" name="banner_img" id="imgInp" class="upload"/>
          </div>

        </span>
                </div>
                <div class="panel-body row"> <img class="col-md-12 topbnr" id="topbanner" name="banner_img" src="../<?php echo $list['banner_img'];?>" alt="your image" /> </div>
            </div>
        </div>
    </div>




    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Mobile App</h3></div>
                <div class="panel-body">

                   



                  

                    <div class="form-group col-md-6">
                        <label for="">Google Play Button</label>
                        <input type="file" class="form-control" name="google_play_btn" id="google_play_btn" placeholder="Android App Store">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="">Google Play App Url</label>
                        <input type="text" class="form-control" name="google_play_url" id="google_play_url" value="<?php echo $list['google_play_url'];?>" placeholder="Google Play App Url">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="">iTunes Button</label>
                        <input type="file" class="form-control" name="itunes_btn" id="itunes_btn" placeholder="iOS App Store">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="">iTunes App Url</label>
                        <input type="text" class="form-control" name="itunes_url" id="itunes_url"  value="<?php echo $list['itunes_url'];?>" placeholder="iTunes App Url">
                    </div>



                </div>
            </div>
        </div>
    </div>


    






</div>

</form>

</section>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#topbanner').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function(){
        readURL(this);
    });







</script>
</body>
</html>
