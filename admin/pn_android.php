<?php
function AndroidPushNotificationCustomer($did, $msg,$ride_id,$ride_status)
{
    // Set POST variables
    $url = 'https://fcm.googleapis.com/fcm/send';

    $app_id="1";

    $fields = array ('to' => $did,'priority' => 'high','data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );

    $headers = array (
        'Authorization: key=AAAATk0dz_A:APA91bHrWy3r_-eYxheNduVuvBA6eyhh4BRXS4fRz8kS3fM85BdBZvtXf-4ZRpU5S5j4Xb9HhPSbWA8QN3cAU17ovv8Kz0BZkRA8m1H-2m0UwQYadzMbLN0T9s_yun3FJqd9xiSxi5lP',
        'Content-Type: application/json' );

    // Open connection
    $ch = curl_init ();
    // Set the url, number of POST vars, POST data
    curl_setopt ( $ch, CURLOPT_URL, $url );
    curl_setopt ( $ch, CURLOPT_POST, true );
    curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
    curl_setopt ( $ch, CURLOPT_POSTFIELDS,  json_encode ( $fields ) );
    // Execute post
    $result = curl_exec ( $ch );
    // Close connection
    curl_close ( $ch );
    return $result;

}

function AndroidPushNotificationDriver($did, $msg,$ride_id,$ride_status) {
    // Set POST variables
    $url = 'https://fcm.googleapis.com/fcm/send';

    $app_id="2";



    $fields = array ('to' => $did,'priority' => 'high','data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );

    $headers = array (
        'Authorization: key=AAAAbtpTzVc:APA91bFo4BnU2mfOOwligyWn4CzVd29DwfK609vwSFFtxLIlOy2Dd68L2lSjH29eGTsrEW9XMYjBAwtfgKprRKZeLGOaWO4yJFnrRV-IWZ9eoaFkRNKx5LNErisOh_b1CR-l2troBJ9w',
        'Content-Type: application/json' );
    // Open connection
    $ch = curl_init ();
    // Set the url, number of POST vars, POST data
    curl_setopt ( $ch, CURLOPT_URL, $url );
    curl_setopt ( $ch, CURLOPT_POST, true );
    curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
    curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
    // Execute post
    $result = curl_exec ( $ch );
    // Close connection
    curl_close ( $ch );
    return $result;
}
?>