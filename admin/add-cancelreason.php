<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

if(isset($_POST['save']))
     {


$con = new mysqli(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
$reason_name = mysqli_real_escape_string($con,$_POST['reason_name']);
$reason_name_other = mysqli_real_escape_string($con,$_POST['reason_name_other']);
$type = mysqli_real_escape_string($con,$_POST['reason_id']);


	$query2="INSERT INTO cancel_reasons (reason_name,reason_type) VALUES ('".$reason_name."','".$type."')";
	$db->query($query2);
 $db->redirect("home.php?pages=add-cancel");
	}

?>
<script>
    function validatelogin() {
        var reason_id = document.getElementById('reason_id').value;
        var reason_name = document.getElementById('reason_name').value;
        if(reason_id == "")
        {
            alert("Select Reason Type");
            return false;
        }
        if(reason_name == "")
        {
            alert("Enter Reason");
            return false;
        }
    }
</script>

<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Cancel Reason</h3>
             
      <span class="tp_rht">
            <a href="home.php?pages=view-cancel" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa fa-reply"></i></a>
      </span>
      
      
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">

          <div class="panel-body">
            <div class="form">
              <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatelogin()">
              
              
              
               <div class="form-group ">
                  <label class="control-label col-lg-2">Please Select Type*</label>
                  <div class="col-lg-6">
                     <select class="form-control" name="reason_id" id="reason_id" >
                        <option value="">--Please Select Reason For--</option>
                         <option value=1>Customer</option>
                         <option value=2>Driver</option>
                         <option value=3>Admin</option>

                          
                    </select>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2">Reason Name*</label>
                  <div class="col-lg-6">
                    <input type="text" class="form-control"  placeholder="Add Reason Name" name="reason_name" id="reason_name" >
                  </div>
                </div>

               

                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Save" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form -->

          </div>
          <!-- panel-body -->
        </div>
        <!-- panel -->
      </div>
      <!-- col -->

    </div>
    <!-- End row -->

  </div>

  <!-- Page Content Ends -->
  <!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
