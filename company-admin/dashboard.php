<?php

include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['Company']['ID']))
{
    $db->redirect("index.php");
}
$companyID=$_SESSION['Company']['ID'];
include('common.php');
$query="select * from user";
$result = $db->query($query);
$ex_rows=$result->num_rows;

$query="select * from driver where company_id='$companyID'";
$result = $db->query($query);
$driver_count=$result->num_rows;

$query="select * from ride_table where company_id='$companyID'";
$result = $db->query($query);
$ex_rows2=$result->num_rows;

$query="select * from company";
$result = $db->query($query);
$ex_rows3=$result->num_rows;

$query="select * from ride_table WHERE ride_status IN (1,3,5,6) AND company_id='$companyID'";
$result = $db->query($query);
$ex_rows6=$result->num_rows;

$query="select * from ride_table WHERE ride_status IN (4,2) AND company_id='$companyID'";
$result = $db->query($query);
$ex_rows7=$result->num_rows;

$query="select * from ride_table WHERE ride_status IN (7) AND company_id='$companyID'";
$result = $db->query($query);
$ex_rows8=$result->num_rows;


?>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Admin Dashboard</h3><br>
        <hr>
    </div>

    <div class="row top col-md-12">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Site Statistics</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                    
                        <a href="home.php?pages=drivers">
                        <div class="col-md-6">
                            <div class="widget-panel widget-style-2 bg-warning">
                                <i class="fa fa-user"></i>
                                <h2 class="m-0 counter"><?php echo $driver_count;?></h2>
                                <div>DRIVERS</div>
                            </div>
                        </div>
                        </a>
                        
                            <?php
                        $sql= "SELECT * FROM payment_confirm INNER JOIN done_ride on payment_confirm.order_id=done_ride.done_ride_id INNER JOIN driver on done_ride.driver_id=driver.driver_id where driver.company_id='$companyID'";
                        $query_sum=$db->query($sql);
                        $result_sum=$query_sum->rows; 
                        $total=0;
                        foreach($result_sum as $payment){
                         $total+=$payment['payment_amount']; 
                        }

                        ?>
                        <a href="home.php?pages=transactions">
                        <div class="col-md-6">
                            <div class="widget-panel widget-style-2 bg-success">
                                <i class="fa fa-usd"></i>
                                <h2 class="m-0 counter"><?php
                                    echo $earning= ($total/1000)." K";
                                    ?></h2>
                                <div>EARNINGS</div>
                            </div>
                        </div>
                        </a>
                    </div>
                    <div class="row">

                        <!--      <div class="col-md-6 ">
                            <div class="widget-panel widget-style-2 bg-danger">
                                <i class="fa fa-building-o"></i>
                                <h2 class="m-0 counter"><?php echo $ex_rows3?></h2>
                                <div>COMPANIES</div>
                            </div>
                        </div> -->
                    <!--    <div class="col-md-6">
                            <div class="widget-panel widget-style-2 bg-info">
                                <i class="fa fa-group"></i>
                                <h2 class="m-0 counter"><?php echo $ex_rows?></h2>
                                <div>RIDERS</div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>

        <!--                    RIDE STATISTICS-->
       
        <div class="col-md-6">
         
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Ride Statistics</h3>
                </div>
               
                <div class="panel-body">
                <a href="home.php?pages=ride-now">
                    <div class="col-md-6 ">
                        <div class="widget-panel widget-style-2 bg-info">
                            <i class="fa fa-cab"></i>
                            <h2 class="m-0 counter"><?php echo $ex_rows2?></h2>
                            <div>TOTAL</div>
                        </div>
                    </div>
                    </a>
                    <a href="home.php?pages=ride-now">
                    <div class="col-md-6">
                        <div class="widget-panel widget-style-2 bg-warning">
                            <i class="fa fa-plane"></i>
                            <h2 class="m-0 counter"><?php echo $ex_rows6?></h2>
                            <div>ON GOING</div>
                        </div>
                    </div>
                   </a>

              <a href="home.php?pages=ride-later">
                    <div class="col-md-6 ">
                        <div class="widget-panel widget-style-2 bg-danger">
                            <i class="fa fa-ban"></i>
                            <h2 class="m-0 counter"><?php echo $ex_rows7?></h2>
                            <div>CANCEL</div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="widget-panel widget-style-2 bg-success">
                            <i class="fa fa-check-circle"></i>
                            <h2 class="m-0 counter"><?php echo $ex_rows8?></h2>
                            <div>Done</div>
                        </div>
                    </div>
</a>
                </div>
            </div>
        </div>
    </div>

    <hr>
</div>
</div>
</section>
</body>
</html>
