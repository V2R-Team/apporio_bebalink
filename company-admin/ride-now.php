<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['Company']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
require 'pn_android.php';
require 'pn_iphone.php';
$company_id = $_SESSION['Company']['ID'];
$query = "select * from ride_table INNER JOIN user ON ride_table.user_id=user.user_id INNER JOIN driver ON ride_table.driver_id=driver.driver_id INNER JOIN car_type ON ride_table.car_type_id=car_type.car_type_id where ride_table.ride_type=1 AND ride_status IN (1,2,3,4,5,6,17) AND driver.company_id='$company_id' ORDER BY ride_table.ride_id DESC";
$result = $db->query($query);
$list=$result->rows;
foreach ($list as $key=>$value)
{
    $payment_option_id = $value['payment_option_id'];
    $query1 = "select * from payment_option where payment_option_id ='$payment_option_id'";
    $result1 = $db->query($query1);
    $list12 = $result1->row;
    $payment_option_name = $list12['payment_option_name'];
    $list[$key]=$value;
    $list[$key]["payment_option_name"]=$payment_option_name;
}

$query = "select * from ride_table INNER JOIN user ON ride_table.user_id=user.user_id INNER JOIN driver ON ride_table.driver_id=driver.driver_id INNER JOIN car_type ON ride_table.car_type_id=car_type.car_type_id where ride_table.ride_type = 2 AND ride_status IN (1,2,3,4,5,6,17) AND driver.company_id='$company_id' ORDER BY ride_table.ride_id DESC";
$result = $db->query($query);
$ride_later=$result->rows;
foreach ($ride_later as $key=>$value)
{
    $payment_option_id = $value['payment_option_id'];
    $query1 = "select * from payment_option where payment_option_id ='$payment_option_id'";
    $result1 = $db->query($query1);
    $list12 = $result1->row;
    $payment_option_name = $list12['payment_option_name'];
    $ride_later[$key]=$value;
    $ride_later[$key]["payment_option_name"]=$payment_option_name;
}
if(isset($_GET['driver_id']) && isset($_GET['ride_id']) && isset($_GET['status'])){
    $driver_id = $_GET['driver_id'];
    $ride_id=$_GET['ride_id'];
    $time1 = date("h:i:s A");
    $query1="UPDATE ride_table SET last_time_stamp='$time1',ride_status=1 WHERE ride_id='$ride_id'";
    $db->query($query1);
    $query1234="select * from driver where driver_id='$driver_id'";
    $result1234 = $db->query($query1234);
    $list1234=$result1234->row;
    $device_id=$list1234['device_id'];

    $query5="INSERT INTO ride_allocated (allocated_ride_id,allocated_driver_id,allocated_ride_status) VALUES ('$ride_id','$driver_id','8')";
    $db->query($query5);



    $message="New Ride Allocated";
    $ride_id= (String) $ride_id;
    $ride_status= "1";
    if($device_id!="")
    {
        if($list1234['flag'] == 1)
        {
            IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status);
        }
        else
        {
            AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
        }
    }
    $db->redirect("home.php?pages=ride-now");
}
$sql = "SELECT * FROM driver WHERE driver_admin_status=1 AND online_offline=1 AND busy=0 AND login_logout=1 AND driver.company_id='$company_id'";
$result3=$db->query($sql);
$drivers=$result3->rows;
$query1234 ="select * from cancel_reasons where cancel_reasons_status=1 AND reason_type=3";
$result1234 = $db->query($query1234);
$list123=$result1234->rows;
?>

<style>
    .clear{clear:both !important;}
    .bookind-id {
        cursor: pointer;
        text-decoration: underline;
    }

</style>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Active Rides</h3>
    </div>
    <div class="row m-t-30">
        <div class="col-sm-12">
            <ul class="nav-tabs navi navi_tabs">
                <li class="active"><a data-toggle="tab" href="#personal"><i class="fa fa-car" aria-hidden="true"></i>
                        <b>Ride Now Bookings</b></a></li>
                <li class=""><a data-toggle="tab" href="#address"><i class="fa fa-clock-o" aria-hidden="true"></i> <b>Ride Later Bookings</b></a></li>
            </ul>
            <div class="panel panel-default p-0">

                <div class="panel-body p-0">

                    <div class="tab-content m-0">
                        <div id="personal" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                        <table id="datatable1" class="table table-striped table-bordered table-responsive">
                                            <thead>
                                            <tr>
                                                <th width="46px">URN&nbsp;&nbsp;<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Unique Reference Number"></i></th>
                                                <th width="5">Rider Name</th>
                                                <th width="5">Driver</th>
                                                <th>Pickup Address</th>
                                                <th>Drop Address</th>
                                                <th width="">Payment Mode</th>
                                                <th width="10%">Ride booked time</th>
                                                <th width="5%">Current Status</th>
                                                <th width="8%">Ride Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($list as $ridenow){?>
                                                <tr>
                                                    <td><a href="home.php?pages=trip-details&id=<?=$ridenow['ride_id']?>" ><span title="Full Details" class="bookind-id"> <?php echo $ridenow['ride_id'];?> </span></a></td>
                                                    <td><?php $user_name = $ridenow['user_name'];
                                                        echo $user_name;
                                                        ?></td>
                                                    <td>
                                                        <?php  $driver_name = $ridenow['driver_name'];
                                                        if($driver_name == "")
                                                        { ?>
                                                            <h4 style="color:red;">Not Assign</h4>

                                                        <?php }else{
                                                            echo $driver_name;
                                                        } ?></td>

                                                    <td>
                                                        <?php
                                                        $pickup_location = $ridenow['pickup_location'];
                                                        echo $pickup_location;
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $drop_location = $ridenow['drop_location'];
                                                        echo $drop_location;
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $payment_option_name = $ridenow['payment_option_name'];
                                                        echo $payment_option_name;
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $ride_date = $ridenow['ride_date'];
                                                        echo $ride_date.",".$ridenow['ride_time'];
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $ride_status = $ridenow['ride_status'];
                                                        $timestap = $ridenow['last_time_stamp'];
                                                        switch ($ride_status){
                                                            case "1":
                                                                echo nl2br("New Booking \n ".$timestap);
                                                                break;
                                                            case "2":
                                                                echo nl2br("Cancelled By User  \n ".$timestap);
                                                                break;
                                                            case "3":
                                                                echo nl2br("Accepted by Driver  \n ".$timestap);
                                                                break;
                                                            case "4":
                                                                echo nl2br("Cancelled by driver  \n ".$timestap);
                                                                break;
                                                            case "5":
                                                                echo nl2br("Driver Arrived  \n ".$timestap);
                                                                break;
                                                            case "6":
                                                                echo nl2br("Trip Started  \n ".$timestap);
                                                                break;
                                                            case "7":
                                                                echo nl2br("Trip Completed  \n ".$timestap);
                                                                break;
                                                            case "8":
                                                                echo nl2br("Trip Book By Admin  \n ".$timestap);
                                                                break;
                                                            case "17":
                                                                echo nl2br("Trip Cancel By Admin  \n ".$timestap);
                                                                break;
                                                            default:
                                                                echo "----";
                                                        }
                                                        ?>
                                                    </td>

                                                    <td align="center">
                                                        <span data-target="#cancel<?php echo $ridenow['ride_id'];?>" data-toggle="modal"><a data-original-title="Cancel Ride"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-times"></i> </a></span>
                                                        <a target="_blank" href="home.php?pages=track-ride&id=<?=$ridenow['ride_id']?>" data-original-title="Track" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="ion-android-locate"></i> </a>



                                                    </td>
                                                </tr>
                                            <?php }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="clear"></div>

                        <div id="address" class="tab-pane">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                        <table id="datatable" class="table table-striped table-bordered table-responsive">
                                            <thead>
                                            <tr>
                                                <th width="46px">URN&nbsp;&nbsp;<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Unique Reference Number"></i></th>
                                                <th width="5">Rider Name</th>
                                                <th width="5">Driver</th>
                                                <th>Pickup Address</th>
                                                <th>Drop Address</th>
                                                <th width="5%">Payment Mode</th>
                                                <th width="10%">Ride booked time</th>
                                                <th width="5%">Current Status</th>
                                                <th width="120px">Ride Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($ride_later as $ride){?>
                                                <tr>
                                                    <td><a href="home.php?pages=trip-details&id=<?=$ride['ride_id']?>" ><span title="Full Details" class="bookind-id"> <?php echo $ride['ride_id'];?> </span></a></td>
                                                    <td><?php $user_name = $ride['user_name'];
                                                        echo $user_name;
                                                        ?></td>
                                                    <td><?php  $driver_name = $ride['driver_name'];
                                                        if($driver_name == "")
                                                        { ?>
                                                            <button  class="btn btn-success"  data-target="#DriverAssign<?php echo $ride['ride_id'];?>" data-toggle="modal" >Assign</button>
                                                        <?php }else{
                                                            echo $driver_name;
                                                        } ?></td>

                                                    <td>
                                                        <?php
                                                        $pickup_location = $ride['pickup_location'];
                                                        echo $pickup_location;
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $drop_location = $ride['drop_location'];
                                                        echo $drop_location;
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $payment_option_name = $ride['payment_option_name'];
                                                        echo $payment_option_name;
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $ride_date = $ride['later_date'];
                                                        echo $ride_date.",".$ride['later_time'];
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $ride_status = $ride['ride_status'];
                                                        $timestap = $ride['last_time_stamp'];
                                                        switch ($ride_status){
                                                            case "1":
                                                                echo nl2br("New Booking \n ".$timestap);
                                                                break;
                                                            case "2":
                                                                echo nl2br("Cancelled By User  \n ".$timestap);
                                                                break;
                                                            case "3":
                                                                echo nl2br("Accepted by Driver  \n ".$timestap);
                                                                break;
                                                            case "4":
                                                                echo nl2br("Cancelled by driver  \n ".$timestap);
                                                                break;
                                                            case "5":
                                                                echo nl2br("Driver Arrived  \n ".$timestap);
                                                                break;
                                                            case "6":
                                                                echo nl2br("Trip Started  \n ".$timestap);
                                                                break;
                                                            case "7":
                                                                echo nl2br("Trip Completed  \n ".$timestap);
                                                                break;
                                                            case "8":
                                                                echo nl2br("Trip Book By Admin  \n ".$timestap);
                                                                break;
                                                            case "17":
                                                                echo nl2br("Trip Cancel By Admin  \n ".$timestap);
                                                                break;
                                                            default:
                                                                echo "----";
                                                        }
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <span data-target="#ridelatercancel<?php echo $ride['ride_id'];?>" data-toggle="modal"><a data-original-title="Cancel Ride"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-times"></i> </a></span>
                                                        <a target="_blank" href="home.php?pages=track-ride&id=<?=$ride['ride_id']?>" data-original-title="Track" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="ion-android-locate"></i> </a>


                                                    </td>
                                                </tr>
                                            <?php }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Assign Driver-->
<?php foreach($ride_later as $ride){?>
    <div class="modal fade"  id="DriverAssign<?php echo $ride['ride_id'];?>" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Assign Driver for Ride Id #<?php echo $ride['ride_id'];?></h4>
                    <div id="assign_driver_success" style="display:none;"> <br>
                        <div class="alert alert-succ$pages->driver_idess"> <strong>Success!</strong> Driver Assigned for Ride Id #<?php echo $ride['ride_id'];?> </div>
                    </div>
                </div>
                <div class="modal-body" >
                    <div class="tab-content">
                        <div id="booking_status_on" class="tab-pane fade in active" style="max-height: 400px; overflow-x: auto;">
                            <table class="table table-striped table-hover table-bordered" id="sample_editable_1" >
                                <tr>
                                    <th>Driver Name</th>
                                    <th>Assign</th>
                                </tr>
                                <?php foreach($drivers as $login) { ?>
                                    <tr>
                                        <td><?php echo $login['driver_name'];?></td>
                                        <td><a href="home.php?pages=ride-now&status=8&driver_id=<?php echo $login['driver_id']?>&ride_id=<?php echo $ride['ride_id']?>" class="" title="Active">
                                                <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" >Assign</button>
                                            </a></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php foreach($ride_later as $ride){?>
    <div class="modal fade"  id="ridelatercancel<?php echo $ride['ride_id'];?>" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cancel Ride #<?php echo $ride['ride_id']; ?></h4>
                </div>
                <form method="get" action="cancel_booking.php">
                    <div class="modal-body" >
                        <div class="tab-content">
                            <div id="booking_status_on" class="tab-pane fade in active" style="max-height: 400px; overflow-x: auto;">
                                <table class="table table-striped table-hover table-bordered" id="sample_editable_1" >
                                    <tbody>
                                    <?php
                                    foreach($list123 as $reasons){?>
                                        <tr><td><input type="radio" name="cancel_reason_id" value="<?php echo $reasons['reason_id']; ?>"> &nbsp; <?php echo $reasons['reason_name']; ?><br></td></tr>
                                    <?php } ?>
                                    <input type="hidden" name="booking_id" value="<?php echo $ride['ride_id'];?>">
                                    <input type="hidden" name="status" value="17">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success br2 btn-xs fs12 activebtn" >Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php foreach($list as $ridenow) { ?>
    <div class="modal fade"  id="cancel<?php echo $ridenow['ride_id'];?>" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cancel Ride #<?php echo $ridenow['ride_id']; ?></h4>
                </div>
                <form method="get" action="cancel_booking.php">
                    <div class="modal-body" >
                        <div class="tab-content">
                            <div id="booking_status_on" class="tab-pane fade in active" style="max-height: 400px; overflow-x: auto;">
                                <table class="table table-striped table-hover table-bordered" id="sample_editable_1" >
                                    <tbody>
                                    <?php
                                    foreach($list123 as $reasons){?>
                                        <tr><td><input type="radio" name="cancel_reason_id" value="<?php echo $reasons['reason_id']; ?>"> &nbsp; <?php echo $reasons['reason_name']; ?><br></td></tr>
                                    <?php } ?>
                                    <input type="hidden" name="booking_id" value="<?php echo $ridenow['ride_id'];?>">
                                    <input type="hidden" name="status" value="17">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success br2 btn-xs fs12 activebtn" >Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
</section>
<!-- Main Content Ends -->

</body></html>