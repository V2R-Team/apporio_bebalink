<?php
session_start();
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
$driver_id = $_GET['driver_id'];
$query = "select * from driver where driver_id='$driver_id'";
$result = $db->query($query);
$list1=$result->row;
$city_id = $list1['city_id'];
$query="SELECT * FROM table_document_list INNER JOIN table_documents ON table_document_list.document_id = table_documents.document_id WHERE city_id='$city_id' ORDER BY city_document_id DESC";
$result=$db->query($query);
$list=$result->rows;
$total_document_need = count($list);
$query1="UPDATE driver SET total_document_need='$total_document_need' WHERE driver_id='$driver_id'";
$db->query($query1);
if (isset($_POST['save']))
{
        foreach($_FILES['document']['tmp_name'] as $key => $tmp_name ) {
            $img_name =  $key.$_FILES['document']['name'][$key];
            $filedir  = "../uploads/driver/";
            if(!is_dir($filedir)) mkdir($filedir, 0755, true);
            $fileext = strtolower(substr($img_name,-4));
            if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
            {
                if($fileext=="jpeg")
                {
                    $fileext=".jpg";
                }
                $pfilename = time().$img_name."document_image_".$driver_id.$fileext;
                $filepath1 = "uploads/driver/".$pfilename;
                $filepath = $filedir.$pfilename;
                copy($_FILES['document']['tmp_name'][$key], $filepath);
                $images_arr[] = $filepath1;
            }
        }
        foreach ($list as $value)
        {
            $document_id = $value['document_id'];
            $document[] = $document_id;
        }

    $totalDoc = count($list);
    for ($i=0;$i<$totalDoc;$i++){
        $docExpiry=$_POST['expire'][$i];
        $docPath = $images_arr[$i];
        $document_id = $document[$i];
        $query2="INSERT INTO table_driver_document(driver_id,document_id,document_path,document_expiry_date) VALUES ('$driver_id','$document_id','$docPath','$docExpiry')";
        $db->query($query2);
    }
    
    
    $msg = "Upload Successfully Successfully";
    echo '<script type="text/javascript">alert("'.$msg.'")</script>';
    $db->redirect("home.php?pages=drivers");

}
?>
<script>
function validatelogin() {
        var driver_doc = document.getElementById('document').value;
        var expire = document.getElementById('expire').value;
        if(driver_doc == "")
        {
            alert("Enter Driver document");
            return false;
        }
         if(expire == "")
        {
            alert("Enter Driver document");
            return false;
        }
       

    }
</script>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Upload Document</h3>

        <span class="tp_rht">
         <a href="home.php?pages=admin" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa fa-reply"></i></a>
      </span>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form">
                        <form class="cmxform form-horizontal tasi-form" enctype="multipart/form-data" method="post"  onSubmit="return validatelogin()">
                            <?php foreach($list as $document){?>
                            <div class="form-group ">
                                <label class="control-label col-lg-2"><?php echo $document['document_name'] ?> *</label>
                                <div class="col-lg-6">
                                    <input type="file" class="form-control"  placeholder="<?php echo $document['document_name'] ?> " name="document[]" id="document" >
                                </div>
                            </div>

                                <div class="form-group ">
                                    <label class="control-label col-lg-2">Expire Date*</label>
                                    <div class="col-lg-6">
                                        <input type="date" class="form-control"  placeholder="Expire Date" name="expire[]" id="expire" >
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Upload" >
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- .form -->

                </div>
                <!-- panel-body -->
            </div>
            <!-- panel -->
        </div>
        <!-- col -->

    </div>
    <!-- End row -->

</div>

<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
