<?php
include_once '../apporioconfig/start_up.php';

 $company_id = $_SESSION['Company']['ID'];

$sqlQuery = "SELECT * FROM company WHERE company_id='$company_id'";

        $rData = $db->query($sqlQuery);
        $res = $rData->row;
        $company_image= $res['company_image'];
       ?>

<aside class="left-panel">
            <div class="logo"> <a href="home.php?pages=ride-now" class="logo-expanded"><img
                                        src="<?php if ($res['company_image'] != '' && isset($res['company_image'])) {
                                            echo '../' . $res['company_image'];
                                        } else {
                                           echo 'images/logo.png';
                                        } ?>" width="100px" height="100px"> <span class="nav-label">Bebalink</span> </a> </div>
            <nav class="navigation">
                <ul class="list-unstyled">
                    <!-- Dashboard Start -->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "dashboard" ) {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=dashboard"><i class="ion-android-home" aria-hidden="true"></i> <span class="nav-label" >Dashboard</span><span class="selected"></span></a></li>



                    <!--Ride Management-->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "ride-now" || $_REQUEST['pages'] == "track-ride" || $_REQUEST['pages'] == "ride-later" || $_REQUEST['pages'] == "trip-details" || $_REQUEST['pages'] == "invoice") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>" id="languagetab"><a href=""><i class="ion-person" aria-hidden="true"></i> <span class="nav-label" >Ride Management&nbsp;&nbsp;<span id="plus" class="fa fa-plus"></span></span><span class="selected"></span></a>
                        <ul class="list-unstyled selected" id="submenu">

                            <?php
                            $arr_open = $color ="";
                            if(@$_REQUEST['pages'] == "ride-now") {
                                $arr_open   = "open";
                                $color = "#9404dd";
                            }
                            ?>

                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=ride-now"><i class="fa fa-caret-right"></i>Active Rides</a></li>
                            <?php
                            $arr_open = $color ="";
                            if(@$_REQUEST['pages'] == "ride-later") {
                                $arr_open   = "open";
                                $color = "#9404dd";
                            }
                            ?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=ride-later"><i class="fa fa-caret-right"></i>Completed Rides</a></li>
                        </ul>
                    </li>

  <!-- user Start 
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "rider" || $_REQUEST['pages'] == "add-rider") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=rider"><i class="ion-android-people" aria-hidden="true"></i> <span class="nav-label" >Riders
  Management</span><span class="selected"></span></a></li> -->
  
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "booking_now") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=booking_now"><i class="ion-android-car" aria-hidden="true"></i> <span class="nav-label" >Manual Taxi Dispatch</span><span class="selected"></span></a></li>

                  <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "rental_booking") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=rental_booking"><i class="ion-android-car" aria-hidden="true"></i> <span class="nav-label" >Rental Manual Dispatch</span><span class="selected"></span></a></li>

                    <!--Driver Management-->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "drivers" || $_REQUEST['pages'] == "accounts" || $_REQUEST['pages'] == "map" || $_REQUEST['pages'] == "pending-driver-approvals" || $_REQUEST['pages'] == "verify-driver") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>" id="languagetab"><a href=""><i class="ion-android-person" aria-hidden="true"></i> <span class="nav-label" >Driver Management&nbsp;&nbsp;<span id="plus" class="fa fa-plus"></span></span><span class="selected"></span></a>
                        <ul class="list-unstyled" id="submenu">

                            <?php
                            $arr_open = $color ="";
                            if(@$_REQUEST['pages'] == "drivers") {
                                $arr_open   = "open";
                                $color = "#9404dd";
                            }
                            ?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=drivers"><i class="fa fa-caret-right"></i>Drivers</a></li>
                            <?php
                            $arr_open = $color ="";
                            if(@$_REQUEST['pages'] == "map") {
                                $arr_open   = "open";
                                $color = "#9404dd";
                            }
                            ?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=map"><i class="fa fa-caret-right"></i>Drivers Map</a></li>
                        </ul>
                    </li>

                    <!-- Transactions Start -->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "transactions") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=transactions"><i class="fa fa-address-book-o" aria-hidden="true"></i> <span class="nav-label" >Transactions</span><span class="selected"></span></a></li>

                    <!--Rental Management-->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "rental-category" || $_REQUEST['pages'] == "rental-car" || $_REQUEST['pages'] == "rental-ride") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>" id="languagetab"><a href=""><i class="fa fa-cc" aria-hidden="true"></i> <span class="nav-label" >Rental Management&nbsp;&nbsp;<span id="plus" class="fa fa-plus"></span></span><span class="selected"></span></a>
                        <ul class="list-unstyled" id="submenu">
                                 <?php
                            $arr_open = $color ="";
                            if(@$_REQUEST['pages'] == "rental-ride") {
                                $arr_open   = "open";
                                $color = "#9404dd";
                            }
                            ?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=rental-ride"><i class="fa fa-caret-right"></i>Rental Rides</a></li>
                        </ul>
                    </li>

                   </ul>
                <ul class="list-unstyled">
                </ul>

            </nav>
        </aside>


